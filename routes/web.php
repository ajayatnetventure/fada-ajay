<?php



Route::get('/', function () {
    return view('dashboard');
});





Route::get('login', 'AdminController@showLogin' );
Route::post('login', 'AdminController@doLogin' );
//end login

//forgot password
Route::post('forgotpassword', 'AdminController@forgotPassword' );
//end forgot password

//logout
Route::get('logout', 'AdminController@doLogout' );
// end logout



//check for session
Route::group(['middleware' => 'usersession'], function () {
    
    Route::get('dashboard', 'AdminController@showDashboard' );

    Route::get('create_usr', 'UsrController@send_to_create_usr' );
    Route::post('create_usr', 'UsrController@post_create_usr' );
    
    
    Route::get('state_data', 'UsrController@give_state_data' );
    Route::get('type_data', 'UsrController@give_type_data' );
    
    
    Route::get('view_usr', 'UsrController@send_to_view_usr' );
    Route::get('view_usr_details/{id}', 'UsrController@send_to_usr_details' );
    
    
    Route::get('update_usr', 'UsrController@send_to_update_usr' );
    Route::post('update_usr', 'UsrController@post_update_usr' );
    
    Route::post('delete_usr', 'UsrController@delete_usr' );
    
    Route::post('changeuserstatus', 'UsrController@changeuserStatus' );
    
    
    
    
    Route::get('creategroup', 'GroupController@showCreateGroup' );
    Route::post('creategroup', 'GroupController@createGroup' );
    
    Route::get('viewgroups', 'GroupController@showGroups' );
    
    Route::post('getfilterelements', 'GroupController@getFilterElements' );
    Route::post('selectmembers', 'GroupController@selectMembers' );
    Route::post('changegroupmemberstatus', 'GroupController@changegroupmemberStatus' );
    Route::post('changegroupstatus', 'GroupController@changegroupStatus' );
    Route::post('changegroupname', 'GroupController@changegroupName' );
    Route::post('changegroupicon', 'GroupController@changegroupIcon' );
    

    //check for super and state admin
    Route::group(['middleware' => 'CheckAdminLevel'], function () {


        Route::get('create_veh', 'VehicleController@SendTo_create_veh' );
        Route::post('create_veh', 'VehicleController@Post_create_veh' );

        Route::get('view_veh', 'VehicleController@SendTo_view_veh' );

        Route::get('update_veh', 'VehicleController@SendTo_update_veh' );
        Route::post('update_veh', 'VehicleController@post_update_veh' );


        Route::get('createfilter', 'FilterController@showFilterForm' );
        Route::post('createfilter', 'FilterController@createFilter' );

        Route::get('addmembers/{id}', 'GroupController@getMembers' );
        Route::post('addmembers/{id}', 'GroupController@addMembers' );

        Route::post('deletefilter', 'FilterController@deleteFilter' );
        Route::post('deletegroup', 'GroupController@deleteGroup' );


        Route::get('create_mfr', 'MfrController@send_to_create_mfr' );
        Route::post('create_mfr', 'MfrController@post_create_mfr' );
        
        Route::get('view_mfr', 'MfrController@send_to_view_mfr' );
        
        Route::get('update_mfr', 'MfrController@send_to_update_mfr' );
        Route::post('update_mfr', 'MfrController@post_update_mfr' );
        
        Route::post('delete_mfr', 'MfrController@delete_mfr' );
        
        Route::post('changemfrstatus', 'MfrController@changemfrStatus' );

        


        Route::get('create_dlr', 'DlrController@send_to_create_dlr' );
        Route::post('create_dlr', 'DlrController@post_create_dlr' );
        
        Route::get('view_dlr', 'DlrController@send_to_view_dlr' );
        
        Route::get('update_dlr', 'DlrController@send_to_update_dlr' );
        Route::post('update_dlr', 'DlrController@post_update_dlr' );
        
        Route::post('delete_dlr', 'DlrController@delete_dlr' );
        
        Route::post('changedlrstatus', 'DlrController@changedlrStatus' );
  
        
        

        Route::get('create_dsg', 'DsgController@send_to_create_dsg' );
        Route::post('create_dsg', 'DsgController@post_create_dsg' );
        
        Route::get('view_dsg', 'DsgController@send_to_view_dsg' );
        
        Route::get('update_dsg', 'DsgController@send_to_update_dsg' );
        Route::post('update_dsg', 'DsgController@post_update_dsg' );
        
        Route::post('delete_dsg', 'DsgController@delete_dsg' );
        
        Route::post('changedsgstatus', 'DsgController@changedsgStatus' );
        
        


        Route::get('create_ad', 'AdminController@send_to_create_ad' );
        Route::post('create_ad', 'AdminController@PostTo_admins' );
        
        Route::get('view_ad_details/{id}', 'AdminController@SendTo_admin_details' );
        Route::get('view_ad', 'AdminController@SendTo_view_ad' );
        
        Route::post('delete_ad', 'AdminController@Delete_ad' );
        
        

        
        Route::get('changepassword', 'AdminController@showchangePassword' );
        Route::post('changepassword', 'AdminController@changePassword' );
        Route::post('changeadminstatus', 'AdminController@changeadminStatus' );



        // Office Bearers
        Route::get('addofficebearers', 'UsrController@showselectofficeBearers' );
        Route::post('addofficebearers', 'UsrController@saveofficeBearers' );
        Route::get('viewofficebearers', 'UsrController@viewofficeBearers' );
        Route::post('officebearerajax', 'UsrController@officebearerAjax' );
        Route::post('deleteofficebearer', 'UsrController@deleteofficeBearer' );
        // End Office Bearers        


    });    

});








//test routes

Route::get('workshop', 'wrokshopController@send_to_workshop' )->middleware('test');
Route::post('workshop', 'wrokshopController@workshop_post' );

Route::get('testend', 'wrokshopController@test_get_response' );
Route::post('testend', 'wrokshopController@delete_mfr' );

Route::get('testend2', 'wrokshopController@test_get_response2' );

//end test routes




