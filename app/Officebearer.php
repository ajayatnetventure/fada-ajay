<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Officebearer extends Model
{
    protected $fillable = [
        'id', 'bearername', 'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    
}
