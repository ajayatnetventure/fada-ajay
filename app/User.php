<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title','firstname','lastname','username', 'email', 'password','mobile','type','manufacturerid','vehicletypeid','dealerid','designationid','address1','address2','cityid','districtid','stateid','pin','STDcode','phonenumber','status','deleted_at','gender','dob'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

     
     
}
