<?php

namespace App\Http\Middleware;

use Closure;

class CheckAdminLevel
{

    public function handle($request, Closure $next)
    {
        $session = $request->session()->get('fada_admin');
        if($session['admintype'] != 0){
            return redirect('dashboard');
        }
        return $next($request);
    }
}
