<?php

namespace App\Http\Middleware;

use Closure;

class CheckUserSession
{

    public function handle($request, Closure $next)
    {
        if (!$request->session()->exists('fada_admin')) {
            return redirect('/login');
        }

        return $next($request);
    }

}
