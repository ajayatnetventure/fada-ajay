<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Session;

use App\Dealer;

class DlrController extends Controller
{
    
    public function send_to_create_dlr(){

        $sidebar = array('section'=>'dlr','activity'=>'create');
        Session::flash('admin-success', $sidebar); 

        return view('create_dlr');
    } 

    public function send_to_view_dlr(){
        
        $sidebar = array('section'=>'dlr','activity'=>'view');
        Session::flash('admin-success', $sidebar);  

         $dlrs = Dealer::all();
         
         return view('view_dlr')->with('dlrs',$dlrs);
  
        
    } 



    public function send_to_update_dlr(Request $request){
        
        $req = $request->All();

        $validator = Validator::make(
            $req,
            [
                'id' => 'required',
            ],
            [
                'id.required' => 'ID is required.',
            ]);
              
            
            if($validator->fails()) 
            {
                return redirect('dashboard');
            }
            else
            {
                $dlrs = Dealer::where('id',$req['id'])->get();
                    
                if(count($dlrs) > 0 )
                {

                    $data = array('id'=>$dlrs[0]['id'],'dlr_name'=>$dlrs[0]['dealername']);
                    return view('update_dlr')->with('data',$data);

                }else
                {
                    return redirect('dashboard');
                }
            }

    } 



    public function post_create_dlr(Request $request){


        $req = $request->All();
        $req['dealername'] = $req['dlr_name'];
        

        $validator = Validator::make(
            $req,
            [
                'dealername' => 'required|unique:dealers',
            ],
            [
                'dealername.required' => 'ID is required.',
            ]);
              
            
            if($validator->fails()) 
            {
                $data['fail']  =  'Could not create dealer';
                return view('create_dlr')->with('data',$data); 
            }
            else
            {
                $dlr_model = new Dealer;

                $dlr_model->dealername = $req['dlr_name'];
                $dlr_model->vehicletype = '';
                $dlr_model->status = 1;

                $dlr_model->save();   
                
                $data['success'] = 'Dealer created successfully';
                return view('create_dlr')->with('data',$data);                
            }            
                           
    }


    

    public function post_update_dlr(Request $request){

        $req = $request->All();
        $req['dealername'] = $req['dlr_name'];
        

        $validator = Validator::make(
            $req,
            [
                'dealername' => 'required|unique:dealers',
            ],
            [
                'dealername.required' => 'ID is required.',
            ]);
            
            
            if($validator->fails()) 
            {
                $data['fail'] = 'dealer could not be updated';
                $data['dlr_name'] = $req['dlr_name'];
                $data['id'] = $req['dlr_id'];

                return view('update_dlr')->with('data',$data);
            }
            else
            {
                $dlr_model = Dealer::find($req['dlr_id']);

                $dlr_model->dealername = $req['dlr_name'];

                $dlr_model->save();
                
                $data['success'] = 'dealer updated successfully';
                $data['dlr_name'] = $req['dlr_name'];
                $data['id'] = $req['dlr_id'];

                return view('update_dlr')->with('data',$data);
            }
     
    }


    public function delete_dlr(Request $request){

        $validator = Validator::make($request->all(), [
            'dlrid' => 'required',
        ],[
            'required' => 'The :attribute is required.',
        ]);
        if ($validator->fails()) {
            return 0;
        }
        $dlr = Dealer::find($request->input('dlrid'));
        $status = $dlr->delete();
        if(!$status){
            return 0;
        }
        return 1;
         
    }

    public function changedlrStatus(Request $request){
        
        $validator = Validator::make($request->all(), [
            'dlrid' => 'required',
        ],[
            'required' => 'The :attribute is required.',
        ]);
        if ($validator->fails()) {
            return 2;
        }
        
        $dlr = Dealer::find($request->input('dlrid'));
        switch($dlr->status) {
            case 0:
               $dlr->status = 1;
               break;
            case 1:
                $dlr->status = 0;
               break;
            }
        $update = $dlr->save();
        if($update){
            return $dlr->status;
        }else{
            return 2;
        }
        
    }


}
