<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use App\Mail\SendAdminMail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Session;
use View;
use App\Admin;
use App\State;
class AdminController extends Controller
{
    public function showLogin(Request $request){
        if ($request->session()->has('fada_admin')) {
            return redirect('dashboard');
        }
        return View::make('login');
    }
    public function doLogin(Request $request){
        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required'
        ],[
            'required' => 'The :attribute is required.',
        ]);
        if ($validator->fails()) {
            $errors= "Both Username and Password are required for login.";
            return view('login')->withErrors($errors);
        }
        $req = $request->All();
        $admin = Admin::where('ad_email', $request->input('username'))->first();
        if(!$admin){
            $errors= 'Invalid Username.';
            return view('login')->withErrors($errors);
        }
        if(!$admin->ad_is_active){
            $errors= 'Your Account is Deactivated.';
            return view('login')->withErrors($errors);
        }
        if(Hash::check($request->input('password'),$admin['ad_password'])){
            $session['sessionid'] = strtotime( date("Y-m-d H:i:s"));
            $session['adminid'] = $admin->id;
            $session['adminusername'] = $admin->ad_user_name;
            $session['adminemail'] = $admin->ad_email;
            $session['datetime'] = date("m-d-Y H:i:s.u");
            $session['ipaddress'] = $request->ip();
            $session['admintype'] = $admin->ad_type;
            $session['is_logged_in'] = TRUE;
            Session::put('fada_admin', $session);
            return redirect('dashboard');
        }else{
            $errors= 'Invalid Credentials.';
            return view('login')->withErrors($errors);
        }
    }
    function forgotPassword(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);
        if ($validator->fails()) {
            $error= "Email is required for setting new password.";
            return view('login')->withErrors($error);
        }
        $admin = Admin::where('ad_email', $request->input('email'))->where('ad_is_active', 1)->first();
        if(!$admin){
            $errors= 'Invalid Email Address.';
            return view('login')->withErrors($errors);
        }
        $password = str_random(10);
        $update['ad_password'] = Hash::make($password);
        $status = $admin->update($update);
        if(!$status){
            $errors= "Cannot create new password. Please try again.";
			return view('login')->withErrors($errors);
        }
        $data['name'] = $admin->ad_user_name;
        $data['password'] = $password;
        Mail::to($request->input('email'))->send(new SendMailable($data));
        \Session::flash('passwordreset', 'Please check your inbox for the new password!');
        return redirect('login');
    }
    function doLogout() {
        Session::flush();
        return redirect('login');
    }
    function showDashboard() {
        
        $sidebar = array('section'=>'none','activity'=>'none');
        Session::flash('admin-success', $sidebar);  
        
        return view('dashboard');
    }
   function send_to_create_ad()
   {   
       $data['states'] = State::All();

       $sidebar = array('section'=>'ad','activity'=>'create');
       Session::flash('admin-success', $sidebar);        

       return view('create_ad')->with('data',$data);
   }
   function PostTo_admins(Request $request)
   {      
        $validator = Validator::make($request->all(), 
        [
            'ad_name' => 'required',
            'ad_email' => 'required|email|unique:admins',
            'ad_type' => 'required',
        ],
        [
            'ad_name.required' => 'Username  is required.',
            'ad_email.required' => 'Email  is required.',
            'ad_email.email' => 'Invalid email format.',
            'ad_email.unique' => 'Email has already been taken.',
            'ad_type.required' => 'Admin type required',
        ]);
        if($validator->fails()) {
            $errors = $validator->errors();
            $data['FailMsg_ad_name'] = $errors->first('ad_name'); 
            $data['FailMsg_ad_email'] = $errors->first('ad_email'); 
            $data['FailMsg_ad_type'] = $errors->first('ad_type'); 
            $data['states'] = State::All();
            return view('create_ad')->with('data',$data); 
        }
        else
        {
            $mfr_model = new Admin;
            if($request->input('ad_type') == 0)
            {
                $mfr_model->ad_type = $request->input('ad_type');
            }
            else
            {
                $mfr_model->ad_type = $request->input('state');                
            }
            $password = str_random(10);
            $mfr_model->ad_user_name = $request->input('ad_name');
            $mfr_model->ad_email = $request->input('ad_email');
            $mfr_model->ad_password = Hash::make($password);
            $mfr_model->ad_is_active = 1;
            $post_response = $mfr_model->save();
            if($post_response)
            {   
                $data['name'] = $request->input('ad_name');
                $data['password'] = $password;
                $data['email'] = $request->input('ad_email');
                Mail::to($request->input('ad_email'))->send(new SendAdminMail($data));
                $data['success_msg'] = 'Admin created successfully';
                $data['states'] = State::All();
                return view('create_ad')->with('data',$data);
            }
            else
            {
                $data['fail_msg'] = 'Counld not create that admin';
                $data['states'] = State::All();
                return view('create_ad')->with('data',$data);  
            }
        }
    }
    function SendTo_admin_details($id)
    {   
        $req['id'] = $id;
        $validator = Validator::make($req, 
        [
            'id' => 'required',
        ],
        [
            'id.required' => 'ID is required.',
        ]);
        if($validator->fails()) 
        {
            $errors = $validator->errors();
            $data['FailMsg_ad_name'] = $errors->first('id'); 
            return $errors;
        }
        else
        {
            $admin = Admin::find($id);
            if($admin['ad_type'] == 1)
            {
                $admin['ad_type'] = 'Super Admin';
            }
            else
            {
                $state = State::find($admin['ad_type']);
                $admin['ad_type'] = 'state admin';
                $admin['ad_state'] =  $state['statename']; 
            }
            return view('admin_details')->with('data',$admin);
        }
    }
    function SendTo_view_ad()
    {   
        //$admins = Admin::where('ad_is_active',1)->get();

        $sidebar = array('section'=>'ad','activity'=>'view');
        Session::flash('admin-success', $sidebar);  

        $admins = Admin::where('ad_type','!=',0)->get();
        return view('view_ad')->with('admins',$admins);
    }
    function Delete_ad(Request $request)
    {  
        $validator = Validator::make($request->all(), [
            'adminid' => 'required',
        ],[
            'required' => 'The :attribute is required.',
        ]);
        if ($validator->fails()) {
            return 0;
        }
        $admin = Admin::find($request->input('adminid'));
        $status = $admin->delete();
        if(!$status){
            return 0;
        }
        return 1;
    }
    public function showchangePassword(){
        return View::make('changePassword'); 
    }

    public function changePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'oldpassword' => 'required',
            'newpassword' => 'required',
            'confirmpassword' => 'required|same:newpassword',
        ],
        [
            'oldpassword.required'    => 'Please fill in the Old Password.',
            'newpassword.required'    => 'Please fill in the New Password.',
            'confirmpassword.required'    => 'Please confirm the New Password.',
            'same'    => 'The New Password and Confirm Password must match.',
        ]
        );
        if ($validator->fails()) {
            $errors = $validator->errors();
            return view('changePassword')->withErrors($errors);
        }
        if (!$request->session()->has('fada_admin')) {
            return redirect('login');
        }
        $session = $request->session()->get('fada_admin');
        $admin = Admin::find($session['adminid']);
        if (!$admin['ad_is_active']) {
            return redirect('login');
        }
        if(Hash::check($request->input('oldpassword'),$admin['ad_password'])){
            $update['ad_password'] = Hash::make($request->input('newpassword'));
            $status = $admin->update($update);
            if(!$status){
                Session::flash("Cannot change password. Please try again.");
                return view('changePassword'); 
            }
            Session::flash('admin-success', 'Your administrator password is changed successfully.');
            return view('changePassword'); 
        }else{
            Session::flash('admin-error', "The Password you entered doesn't match our records.");
            return view('changePassword'); 
        }
    }

    public function changeadminStatus(Request $request){
        
        $validator = Validator::make($request->all(), [
            'adminid' => 'required',
        ],[
            'required' => 'The :attribute is required.',
        ]);
        if ($validator->fails()) {
            return 2;
        }
        $admin = Admin::find($request->input('adminid'));
        switch($admin->ad_is_active) {
            case 0:
               $admin->ad_is_active = 1;
               break;
            case 1:
                $admin->ad_is_active = 0;
               break;
            }
        $update = $admin->save();
        if($update){
            return $admin->ad_is_active;
        }else{
            return 2;
        }
        
    }

}
