<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Manufacturer;
use Session;

use App\Vehicle;

class MfrController extends Controller
{


    public function send_to_create_mfr(){
        $data['vehicles'] = Vehicle::All();

        $sidebar = array('section'=>'mfr','activity'=>'create');
        Session::flash('admin-success', $sidebar); 

        return view('create_mfr')->with('data',$data);
    } 


    public function send_to_view_mfr(){
        $vehicle = Vehicle::where('status',1)->get();
        $mfrs = Manufacturer::all();
        foreach($mfrs as $key=>$value){
            $ids=array();
            $type = json_decode($value->vehicletype,true);
            foreach($type as $t){
                foreach($vehicle as $v){
                    if ($t == $v['id']) {
                        $ids[] = $v['vehicletype'];
                    }
                }
            }
            $mfrs[$key]['type'] = $ids;
        }

        $sidebar = array('section'=>'mfr','activity'=>'view');
        Session::flash('admin-success', $sidebar); 

        return view('view_mfr')->with('mfrs',$mfrs);
    }
    
    
    public function send_to_update_mfr(Request $request){
        $req = $request->All();
        if(!empty($req['id']))
        {
            $mfrs = Manufacturer::where('id',$req['id'])->get();
            if(!count($mfrs)){
                Session::flash('mfr-view-error', "Selected manufacturer doesn't exist.");
                return redirect('view_mfr');
            }
            $data = array();
            $data['id'] = $mfrs[0]['id'];
            $data['mfr_name'] = $mfrs[0]['manufacturername'];
            $data['vehicles'] = Vehicle::All();
            $data['selected_veh'] = json_decode($mfrs[0]['vehicletype']);
            return view('update_mfr')->with('data',$data);
        }
        else
        { 
            Session::flash('mfr-view-error', "Please select the manufacturer you want to update.");
            return redirect('view_mfr');
        }
    } 


  

    public function post_create_mfr(Request $request){
        $data['vehicles'] = Vehicle::All();
        $validator = Validator::make($request->all(), [
            'mfr_name' => 'required',
            'vehicle_type' => 'required',
        ],[
            'required' => 'The :attribute is required.',
        ]);
        if ($validator->fails()) {
            Session::flash('mfr-error', 'Please select the manufacturer name and type.');
            return view('create_mfr')->with('data',$data);
        }
        $mfr_model = new Manufacturer;
        $mfr_model->manufacturername = $request->input('mfr_name');
        $mfr_model->vehicletype = json_encode($request->input('vehicle_type'));
        $mfr_model->status = 1;
        $status = $mfr_model->save();   
        if(!$status){
            Session::flash('mfr-error', 'Cannot create new manufacturer.');
            return view('create_mfr')->with('data',$data);  
        }
        Session::flash('mfr-success', 'New manufacturer created successfully.');
        return view('create_mfr')->with('data',$data);
    }


    public function post_update_mfr(Request $request){
        
        $data['vehicles'] = Vehicle::All();
        $validator = Validator::make($request->all(), [
            'mfr_name' => 'required',
            'vehicle_type' => 'required',
        ],[
            'required' => 'The :attribute is required.',
        ]);
        if ($validator->fails()) {
            Session::flash('mfr-error', 'Please select the manufacturer name and type.');
            return view('update_mfr')->with('data',$data);
        }
        $mfr_model = Manufacturer::find($request->input('mfr_id'));
        $mfr_model->manufacturername = $request->input('mfr_name');
        $mfr_model->vehicletype = json_encode($request->input('vehicle_type'));
        $mfr_model->status = 1;
        $status = $mfr_model->save();   
        if(!$status){
            Session::flash('mfr-error', 'Cannot update manufacturer.');
            return view('update_mfr')->with('data',$data); 
        }
        Session::flash('mfr-success', 'Manufacturer updated successfully.');
        $data['id'] = $request->input('mfr_id');
        $data['mfr_name'] = $request->input('mfr_name');
        $data['selected_veh'] = $request->input('vehicle_type');
        return view('update_mfr')->with('data',$data);
    }

    public function delete_mfr(Request $request){
        $validator = Validator::make($request->all(), [
            'mfrid' => 'required',
        ],[
            'required' => 'The :attribute is required.',
        ]);
        if ($validator->fails()) {
            return 0;
        }
        $mfr = Manufacturer::find($request->input('mfrid'));
        $status = $mfr->delete();
        if(!$status){
            return 0;
        }
        return 1;
         
        
    }

    public function changemfrStatus(Request $request){
        
        $validator = Validator::make($request->all(), [
            'mfrid' => 'required',
        ],[
            'required' => 'The :attribute is required.',
        ]);
        if ($validator->fails()) {
            return 2;
        }
        
        $mfr = Manufacturer::find($request->input('mfrid'));
        switch($mfr->status) {
            case 0:
               $mfr->status = 1;
               break;
            case 1:
                $mfr->status = 0;
               break;
            }
        $update = $mfr->save();
        if($update){
            return $mfr->status;
        }else{
            return 2;
        }
        
    }




}
