<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Session;

use App\User;
use App\City;
use App\State;
use App\Vehicle;
use DB;
use App\Manufacturer;
use App\Dealer;
use App\Designation;
use View;



use DateTime;


class UsrController extends Controller
{
     
    public function send_to_create_usr(Request $request){
        $session = $request->session()->get('fada_admin');
        if($session['admintype'] == 0){
            $data['states'] = State::All();
        }else{
            $data['states'] = State::where('id',$session['admintype'])->get();
        }
        $data['manufacturers'] = Manufacturer::All();
        $data['dealers'] = Dealer::All();
        $data['designations'] = Designation::All(); 
         
        $sidebar = array('section'=>'usr','activity'=>'create');
        Session::flash('admin-success', $sidebar);         
        
        return view('create_usr')->with('data',$data);
        
        
    } 

    public function give_state_data(Request $request){
        
        $req = $request->All();
            
        $city = City::where('stateid',$req['state_id'])->get();
        return $city;
        
    }



    public function give_type_data(Request $request){

        $req = $request->All();

        $mfr_model = Manufacturer::find($req['mfr_id']);
        $data = json_decode($mfr_model['vehicletype']);

        for($i = 0;$i < count($data);$i++)
        {
           $veh_type[$i]['veh_type_id'] = $data[$i];

           $veh_model = Vehicle::find($data[$i]);
           $veh_type[$i]['veh_type_name'] = $veh_model['vehicletype'];
        }

        return $veh_type;
        
    }

    public function post_create_usr(Request $request){
        $req = $request->All();
        $validator = Validator::make(
            $req,
            [
                'user_name' => 'required',
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required',
                'mobile_no' => 'required',
                'password' => 'required',
                'manufacture_type' => 'required',
                'vehicle_type' => 'required',
                'dealer_type' => 'required',
                'address1' => 'required',
                'address2' => 'required',
                'city' => 'required',
                'state' => 'required',
                'pin' => 'required',
                'std_code' => 'required',
                'phone' => 'required',
                'gender' => 'required',
                'dob' => 'required',
            ]);
        if($validator->fails()) 
        {
           return 'All flields are required.';//change this
        }
        else
        {
           

            $usr_model =  array();
            
            $usr_model['username'] = $req['user_name'];
            $usr_model['title'] = 'test_title';
            $usr_model['firstname'] = $req['first_name'];
            $usr_model['lastname'] = $req['last_name'];
            $usr_model['email'] = $req['email'];
            $usr_model['mobile'] = $req['mobile_no'];
            $usr_model['password'] = $req['password'];
            $usr_model['type'] = 'test_type';
            $usr_model['manufacturerid'] = $req['manufacture_type'];
            $usr_model['vehicletypeid'] = $req['vehicle_type']; //change 
            $usr_model['dealerid'] = $req['dealer_type'];
            $usr_model['designationid'] = $req['designation_type'];
            $usr_model['address1'] = $req['address1'];
            $usr_model['address2'] = $req['address2'];
            $usr_model['cityid'] = $req['city'];
            $usr_model['stateid'] = $req['state'];
            $usr_model['pin'] = $req['pin'];
            $usr_model['STDcode'] =  $req['std_code'];
            $usr_model['phonenumber'] =  $req['phone'];
            $usr_model['status'] = 1;
            // $usr_model->deleted_at = "";
            $usr_model['gender'] = $req['gender'];
            $usr_model['dob'] = $req['dob'];

            $usr_model['districtid'] = 1;

            $response = User::create($usr_model);
        
            
            
            if($response)
            {
                $curl = curl_init();

                $post = [
                "user_id" => 'fada_user_'.$response->id,
                "nickname" => $response->username,
                "profile_url"  => '',            
                "issue_access_token"=> false
                ];
                
                curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.sendbird.com/v3/users",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($post),
                CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json, charset=utf8",
                "Api-Token: 0b999a94f8d57e44283235257a18c96855072d9c"
                ),
                ));
                
                $response = curl_exec($curl);
                
                $err = curl_error($curl);
                
                curl_close($curl);


                $data['message'] = 'created successfully';
                $data['states'] = State::All();
                $data['manufacturers'] = Manufacturer::All();
                $data['dealers'] = Dealer::All();
                $data['designations'] = Designation::All();           
        
                return view('create_usr')->with('data',$data);        
    

            }
            else
            {
                return 'failed'; //change
            }
                

        }

            
        
    }     


    public function send_to_view_usr(Request $request){
        $session = $request->session()->get('fada_admin');
        $users = DB::table('users')
            ->select(
                'users.id',
                'users.username',
                'users.title',
                'users.firstname',
                'users.lastname',
                'users.email',
                'users.mobile',
                'manufacturers.manufacturername as manufacturer',
                'dealers.dealername as dealer',
                'designations.designationname as designation',
                'users.address1',
                'users.address2',
                'cities.cityname as city',
                'districts.districtname as district',
                'states.statename as state',
                'users.pin',
                'users.type',
                'users.status'
            )
            ->join(
                'manufacturers',
                'manufacturers.id','=','users.manufacturerid'
            )
            ->join(
                'dealers',
                'dealers.id','=','users.dealerid'
            )
            ->join(
                'designations',
                'designations.id','=','users.designationid'
            )
            ->join(
                'cities',
                'cities.id','=','users.cityid'
            )
            ->join(
                'districts',
                'districts.id','=','users.districtid'
            )
            ->join(
                'states',
                'states.id','=','users.stateid'
            )
            ->where('deleted_at',NULL);
            if($session['admintype'] != 0){
                $users = $users->where('users.stateid',$session['admintype']);
            }
            $users = $users->get();
        $sidebar = array('section'=>'usr','activity'=>'view');
        Session::flash('admin-success', $sidebar); 
        return view('view_usr')->with('users',$users);
        
    } 


    public function delete_usr(Request $request){
        $validator = Validator::make($request->all(), [
            'userid' => 'required',
        ],[
            'required' => 'The :attribute is required.',
        ]);
        if ($validator->fails()) {
            return 0;
        }
        $user = User::find($request->input('userid'));
        if(!$user){
            return 0;
        }
        $user->deleted_at = new DateTime();
        $user->status = 0;
        $status = $user->save();
        if(!$status){
            return 0;
        }
        return 1;
         
        
    }


    public function send_to_update_usr(Request $request){
        $session = $request->session()->get('fada_admin');
        
        $req = $request->All();

        $validator = Validator::make($req,['id' => 'required',],['id.required' => 'ID is required.',]);

        if($validator->fails()) 
        {
            return ' validation error';//change this
        }
        else
        {
            $usrs = array();
            if ($session['admintype'] == 0) {
                $usrs = User::where('id', $req['id'])->get();
            }else{
                $usrs = User::where('id', $req['id'])->where('stateid', $session['admintype'])->get();  
            }
            if(!count($usrs)){
                return redirect('view_usr');
            }
            if($session['admintype'] == 0){
                $usrs[0]['states'] = State::All();
            }else{
                $usrs[0]['states'] = State::where('id',$session['admintype'])->get();
            }
            $usrs[0]['manufacturers'] = Manufacturer::All();
            $usrs[0]['dealers'] = Dealer::All();
            $usrs[0]['designations'] = Designation::All();          
            
            return view('update_usr')->with('data',$usrs[0]);
        }        

    } 



    public function post_update_usr(Request $request){


        $req = $request->All();

        $validator = Validator::make(
            $req,
            [
                'id' => 'required',
                'user_name' => 'required',
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required',
                'mobile_no' => 'required',
                'password' => 'required',
                'manufacture_type' => 'required',
                'vehicle_type' => 'required',
                'dealer_type' => 'required',
                'address1' => 'required',
                'address2' => 'required',
                'city' => 'required',
                'state' => 'required',
                'pin' => 'required',
                'std_code' => 'required',
                'phone' => 'required',
                'gender' => 'required',
                'dob' => 'required',
            ]);

            
        if($validator->fails()) 
        {
           return 'All flields are required.';//change this
        }
        else
        {

            $usr_model = User::find($req['id']);

            $usr_model->username = $req['user_name'];
            $usr_model->title = 'changed_title';
            $usr_model->firstname =  $req['first_name'];
            $usr_model->lastname = $req['last_name'];
            $usr_model->email = $req['email'];
            $usr_model->mobile = $req['mobile_no'];
            $usr_model->password = $req['password'];
            $usr_model->type = 'changed_type';
            $usr_model->manufacturerid = $req['manufacture_type'];
            $usr_model->vehicletypeid = $req['vehicle_type'];
            $usr_model->dealerid = $req['dealer_type'];
            $usr_model->designationid = $req['designation_type'];
            $usr_model->address1 = $req['address1'];
            $usr_model->address2 = $req['address2'];
            $usr_model->cityid = $req['city'];
            $usr_model->stateid = $req['state'];
            $usr_model->pin = $req['pin'];
            $usr_model->STDcode = $req['std_code'];
            $usr_model->phonenumber = $req['phone'];
            $usr_model->status = 1;
            // $usr_model->deleted_at = new DateTime();
            $usr_model->gender =  $req['gender'];
            $usr_model->dob = $req['dob'];

            $usr_model->districtid = 1;
            
            $response = $usr_model->save();   
            
        
            if($response)
            {
                $data['message'] = 'Manufacturer created successfully';
            
                $data['id'] = $req['id'];
                $data['username'] = $req['user_name'];
                $data['title'] = 'changed_title';
                $data['firstname'] =  $req['first_name'];
                $data['lastname'] = $req['last_name'];
                $data['email'] = $req['email'];
                $data['mobile'] = $req['mobile_no'];
                $data['password'] = $req['password'];
                $data['type'] = 'changed_type';
                $data['manufacturerid'] = $req['manufacture_type'];
                $data['vehicletypeid'] = $req['vehicle_type'];
                $data['dealerid'] = $req['dealer_type'];
                $data['designationid'] = $req['designation_type'];
                $data['address1'] = $req['address1'];
                $data['address2'] = $req['address2'];
                $data['cityid'] = $req['city'];
                $data['stateid'] = $req['state'];
                $data['pin'] = $req['pin'];
                $data['STDcode'] = $req['std_code'];
                $data['phonenumber'] = $req['phone'];
                $data['status'] = 0;
                // $data['deleted_at'] = new DateTime();
                $data['gender'] = $req['gender'];
                $data['dob'] = $req['dob'];
    
                $data['message'] = 'updated successfully';
                $data['states'] = State::All();
                $data['manufacturers'] = Manufacturer::All();
                $data['dealers'] = Dealer::All();
                $data['designations'] = Designation::All();   
    
                return view('update_usr')->with('data',$data);
                
                
            }  
            else{
              
                return 'req not set correctly';
            }   


        }    

       
    }



    public function send_to_usr_details($id){

        $req['id'] = $id;
        $validator = Validator::make($req,['id' => 'required',],['id.required' => 'ID is required.',]);

        if($validator->fails()) 
        {
            return 'error';//change this
        }
        else
        {
            $usr = User::where('id',$req['id'])->get();
             
            $manufacturer =   Manufacturer::where('id',$usr[0]['manufacturerid'])->get();
            $dealer =   Dealer::where('id',$usr[0]['dealerid'])->get();
            $designation =   Designation::where('id',$usr[0]['designationid'])->get();
            
            $state =   State::where('id',$usr[0]['stateid'])->get();
            $city =   City::where('id',$usr[0]['cityid'])->get();
            
            $usr[0]['manufacturename'] = $manufacturer[0]['manufacturername'];
            $usr[0]['dealername'] = $dealer[0]['dealername'];
            $usr[0]['designationname'] = $designation[0]['designationname'];
             
            $usr[0]['statename'] = $state[0]['statename'];
            $usr[0]['cityname'] = $city[0]['cityname'];
             
            return view('usr_details')->with('data',$usr);         
            //return $usr; 
            

        }
        /* else{
            return 'req not set property';
        } */

    } 

    public function changeuserStatus(Request $request){
        
        $validator = Validator::make($request->all(), [
            'userid' => 'required',
        ],[
            'required' => 'The :attribute is required.',
        ]);
        if ($validator->fails()) {
            return 2;
        }
        
        $user = User::find($request->input('userid'));
        switch($user->status) {
            case 0:
                $post = [
                    'freeze' => "FALSE"
                ];
               $user->status = 1;
               break;
            case 1:
            $post = [
                'freeze' => true
            ];
                $user->status = 0;
               break;
            }
        $update = $user->save();
        if($update){
            
            /* $url = "https://api.sendbird.com/v3/group_channels/".$group->channelurl."/freeze";
            $curl = curl_init();
            
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "PUT",
                CURLOPT_POSTFIELDS => json_encode($post),
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json, charset=utf8",
                    "Api-Token: 0b999a94f8d57e44283235257a18c96855072d9c"
                ),
            ));
            
            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl); */
            return $user->status;
        }else{
            return 2;
        }
        
    }

    public function showselectofficeBearers(Request $request){

        $sidebar = array('section'=>'ofc','activity'=>'create');
        Session::flash('admin-success', $sidebar);         
        
        
        $data = DB::table('users')
            ->select(
                'users.id',
                'users.username',
                'users.title',
                'users.firstname',
                'users.lastname',
                'users.email',
                'users.mobile',
                'manufacturers.manufacturername as manufacturer',
                'vehicles.vehicletype',
                'dealers.dealername as dealer',
                'designations.designationname as designation',
                'users.address1',
                'users.address2',
                'cities.cityname as city',
                'districts.districtname as district',
                'states.statename as state',
                'users.pin',
                'users.type',
                'states.id as stateid'
            )
            ->join(
                'manufacturers',
                'manufacturers.id','=','users.manufacturerid'
            )
            ->join(
                'dealers',
                'dealers.id','=','users.dealerid'
            )
            ->join(
                'vehicles',
                'vehicles.id','=','users.vehicletypeid'
            )
            ->join(
                'designations',
                'designations.id','=','users.designationid'
            )
            ->join(
                'cities',
                'cities.id','=','users.cityid'
            )
            ->join(
                'districts',
                'districts.id','=','users.districtid'
            )
            ->join(
                'states',
                'states.id','=','users.stateid'
            )
            ->where(['users.status' => 1])
            ->get();
        $states = State::All();
        $array = array('users'=>$data,'states'=>$states);
        return View::make('addofficebearers')->with($array);
    }

    public function saveofficeBearers(Request $request){
        $validator = Validator::make($request->all(), [
            'bearer_title' => 'required',
            'bearer_type' => 'required',
            'user' => 'required',
        ],[
            'required' => 'The :attribute is required.',
        ]);
        if ($validator->fails()) {
            Session::flash('member-error', 'Please select all the fields.'); 
            return redirect('addofficebearers');
        }
        $user = User::find($request->input('user'));
        switch($request->input('bearer_type')) {
            case 0:
                $user->is_national = 0;
                break;
            case 1:
                $user->is_national = 1;
               break;
            }
        $user->is_officebearer = $request->input('bearer_title');
        $update = $user->save();
        if(!$update){
            Session::flash('member-error', 'Office bearer cannot be added. Please try again');
            return redirect('addofficebearers');
        }
        Session::flash('member-success', 'Office bearer added successfully');
        return redirect('addofficebearers');


    }

    public function viewofficeBearers(Request $request){

        $sidebar = array('section'=>'ofc','activity'=>'view');
        Session::flash('admin-success', $sidebar);
        
        $states = State::All();
        $array = array('states'=>$states);
        return View::make('viewofficebearers')->with($array);
    }

    public function officebearerAjax(Request $request){
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'type' => 'required',
        ],[
            'required' => 'The :attribute is required.',
        ]);
        if ($validator->fails()) {
            return 0;
        }
        switch($request->input('type')) {
            case 0:
                $data = User::where('is_officebearer',$request->input('title'))->where('stateid',$request->input('state'))->where('is_national',0)->get();
                return json_encode($data);
               break;
            case 1:
                $data = User::where('is_officebearer',$request->input('title'))->where('is_national',$request->input('type'))->get();
                return json_encode($data);
               break;
            }
    }

    public function deleteofficeBearer(Request $request){
        $validator = Validator::make($request->all(), [
            'id' => 'required',
        ],[
            'required' => 'The :attribute is required.',
        ]);
        if ($validator->fails()) {
            return 0;
        }
        $user = User::find($request->input('id'));
        if(!$user){
            return 0;   
        }
        $user->is_national = NULL;
        $user->is_officebearer = NULL;
        $update = $user->save();
        if (!$update) {
            return 0;
        }
        return 1;
    }



}
