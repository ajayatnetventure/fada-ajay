<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Session;

use App\Vehicle;


class VehicleController extends Controller
{

    public function SendTo_create_veh()
    {
        $sidebar = array('section'=>'veh','activity'=>'create');
        Session::flash('admin-success', $sidebar); 

        return view('create_veh');
    }

    public function Post_create_veh(Request $request)
    {   

        $req = $request->All();
        $req['vehicletype'] = $req['veh_name'];
        

        $validator = Validator::make(
            $req,
            [
                'vehicletype' => 'required|unique:vehicles',
            ],
            [
                'vehicletype.required' => 'ID is required.',
            ]);
              
            
            if($validator->fails()) 
            {
                $data['fail']  =  'Could not create Vehicle Type';
                return view('create_veh')->with('data',$data); 
            }
            else
            {
                $dlr_model = new Vehicle;

                $dlr_model->vehicletype = $req['veh_name'];
                $dlr_model->status = 1;

                $dlr_model->save();   
                
                $data['success'] = 'Dealer created successfully';
                return view('create_veh')->with('data',$data);                
            }  
            
    }

    public function SendTo_view_veh()
    {   
        $sidebar = array('section'=>'veh','activity'=>'view');
        Session::flash('admin-success', $sidebar); 

        $data = Vehicle::where('status',1)->get();

        return view('view_veh')->with('data',$data);
    }


    public function SendTo_update_veh(Request $request)
    {   
        $req = $request->All();

        $validator = Validator::make(
            $req,
            [
                'id' => 'required',
            ],
            [
                'id.required' => 'ID is required.',
            ]);
              
            
            if($validator->fails()) 
            {
                return redirect('dashboard');
            }
            else
            {
                $data['vehicle'] = Vehicle::find($req['id']);
                return view('update_veh')->with('data',$data);
            }    
    }    


    public function post_update_veh(Request $request)
    {   
        $req = $request->All();
        $req['vehicletype'] = $req['veh_name'];       

        $validator = Validator::make(
            $req,
            [
                'vehicletype' => 'required|unique:vehicles',
            ],
            [
                'vehicletype.required' => 'ID is required.',
            ]);
              
            
            if($validator->fails()) 
            {   
                $data['vehicle'] = Vehicle::find($req['id']);
                $data['fail']  =  'Could not create Vehicle Type';
                return view('update_veh')->with('data',$data);
            }
            else
            {   
                $data['vehicle'] = Vehicle::find($req['id']);
                $data['success'] = 'Vehicle Type created successfully';
                return view('update_veh')->with('data',$data);
            }    
    }  



}
