<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Session;

use App\Designation;


class DsgController extends Controller
{

    public function send_to_create_dsg(){

        $sidebar = array('section'=>'dsg','activity'=>'create');
        Session::flash('admin-success', $sidebar); 

        return view('create_dsg');
    } 

    
    public function post_create_dsg(Request $request){

        $req = $request->All();
        $req['designationname'] = $req['dsg_name'];
        
        $validator = Validator::make(
            $req,
            [
                'designationname' => 'required|unique:designations',
            ],
            [
                'designationname.required' => 'ID is required.',
            ]);
                
        if($validator->fails()) 
        {
            

            $data['fail'] = 'Designation could not be created';
            return view('create_dsg')->with('data',$data);            
        }
        else
        {
            $dsg_model = new Designation;

            $dsg_model->designationname = $req['dsg_name'];
            $dsg_model->status = 1;

            $dsg_model->save();   
            
            $data['success'] = 'Designation created successfully';
            return view('create_dsg')->with('data',$data);
        }

    } 


    public function send_to_update_dsg(Request $request){

        $req = $request->All();

        $validator = Validator::make(
            $req,
            [
                'id' => 'required',
            ],
            [
                'id.required' => 'ID is required.',
            ]);

        if($validator->fails()) 
        {
            return ' validation error';//change this
        }
        else
        {
            $dsgs = Designation::where('id',$req['id'])->get();
                  
            if(count($dsgs) > 0 )
            {
                $data = array('id'=>$dsgs[0]['id'],'dsg_name'=>$dsgs[0]['designationname']);
                return view('update_dsg')->with('req',$data);
            }else
            {
                return 'no Designation with that id';
            }   
        }        

    } 



    public function send_to_view_dsg(){
        
        $dsgs = Designation::all();

        $sidebar = array('section'=>'dsg','activity'=>'view');
        Session::flash('admin-success', $sidebar);         

        return view('view_dsg')->with('dsgs',$dsgs);
        
    } 

    public function delete_dsg(Request $request){
        $validator = Validator::make($request->all(), [
            'dsgid' => 'required',
        ],[
            'required' => 'The :attribute is required.',
        ]);
        if ($validator->fails()) {
            return 0;
        }
        $dsg = Designation::find($request->input('dsgid'));
        $status = $dsg->delete();
        if(!$status){
            return 0;
        }
        return 1;
         
        
    }




    public function post_update_dsg(Request $request){

        $req = $request->All();

        $validator = Validator::make(
            $req,
            [
                'dsg_id' => 'required',
                'dsg_name' => 'required',
            ],
            [
                'dsg_id.required' => 'ID is required.',
                'dsg_name.required' => 'Name is required.',
            ]);

        if($validator->fails()) 
        {
            return ' validation error';//change this
        }
        else
        {

            $dsg_model = Designation::find($req['dsg_id']);

            $dsg_model->designationname = $req['dsg_name'];

            $dsg_model->save();   
            
            $data['message'] = 'dealer updated successfully';
            $data['dsg_name'] = $req['dsg_name'];
            $data['id'] = $req['dsg_id'];

            return view('update_dsg')->with('req',$data);            

        }       
           
    }

    public function changedsgStatus(Request $request){
        
        $validator = Validator::make($request->all(), [
            'dsgid' => 'required',
        ],[
            'required' => 'The :attribute is required.',
        ]);
        if ($validator->fails()) {
            return 2;
        }
        
        $dsg = Designation::find($request->input('dsgid'));
        switch($dsg->status) {
            case 0:
               $dsg->status = 1;
               break;
            case 1:
                $dsg->status = 0;
               break;
            }
        $update = $dsg->save();
        if($update){
            return $dsg->status;
        }else{
            return 2;
        }
        
    }






}
