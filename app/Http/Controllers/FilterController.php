<?php

namespace App\Http\Controllers;
use View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Filter;
use Session;

class FilterController extends Controller
{
    public function showFilterForm(){
 
        $sidebar = array('section'=>'none','activity'=>'none');
        Session::flash('admin-success', $sidebar);  
        
        $success = FALSE;
        $datas = Filter::where('status',1)->get();
        $array = array('success'=>$success, 'datas'=>$datas);
        return View::make('createFilter')->with($array);
    }

    public function createFilter(Request $request){
        $validator = Validator::make($request->all(), [
            'filterdata' => 'required',
        ],[
            'required' => 'The :attribute is required.',
        ]);
        $success= FALSE;
        $datas = Filter::where('status',1)->get();
        $array = array('success'=>$success, 'datas'=>$datas);
        if ($validator->fails()) {
            $errors= "Filter is required.";
            return view('createFilter')->withErrors($errors)->with($array);
        }
        $data = Filter::where('filterdata',$request->input('filterdata'))->where('status',1)->get();
        if (count($data) !=0 ) {
            $errors= "Selected Filter already exists.";
            return view('createFilter')->withErrors($errors)->with($array);
        }
        $filter = new Filter;
        $filter->filterdata = $request->input('filterdata');
        $filter->status = 1; 
        switch ($request->input('filterdata')) {
            case 'Manufacturer(Make)':
                $filter->filtertable = 'Manufacturer';
                break;
            case 'Vehicle Type':
                $filter->filtertable = 'Vehicle';
                break;
            case 'Designation of Member':
                $filter->filtertable = 'Designation';
                break;
            case 'Dealer':
                $filter->filtertable = 'Dealer';
                break;
            case 'State':
                $filter->filtertable = 'State';
                break;
            case 'City':
                $filter->filtertable = 'City';
                break;
            case 'Office Bearers':
                $filter->filtertable = 'Officebearers';
                break;
           
        }
        $filter->save();
        $success = "New Filter Added.";
        $datas = Filter::where('status',1)->get();
        $array = array('success'=>$success, 'datas'=>$datas);
        return view('createFilter')->with($array);
    }

    public function deleteFilter(Request $request){

        $data = Filter::find($request->input('id'));
        if (!$data) {
            return 0;
        }
        $data->status = 0; 
        $data->save();
        return 1;
    }
}
