<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use DB;
use View;
use Illuminate\Http\Request;
use App\Filter;
use App\Manufacturer;
use App\Designation;
use App\Dealer;
use App\State;
use App\Vehicle;
use App\City;
use App\User;
use App\Group;
use App\Groupuser;
use App\Officebearer;
use Session;

class GroupController extends Controller
{
    public function showCreateGroup(){

        $sidebar = array('section'=>'grp','activity'=>'create');
        Session::flash('admin-success', $sidebar);   
        
        $filters = Filter::where('status',1)->get();
        $array = array('filters'=>$filters);
        return View::make('groupFilter')->with($array);
        
        
    }

    public function getFilterElements(Request $request){
        $session = $request->session()->get('fada_admin');
        $filter = Filter::find($request->input('filter'));
        if (!$filter) {
            return FALSE;
        }
        switch ($filter['filterdata']) {
            case 'Manufacturer(Make)':
                $data = Manufacturer::select('id', 'manufacturername as name')->where('status',1)->orderBy('manufacturername', 'ASC')->get();
                break;
            case 'Vehicle Type':
                $data = Vehicle::select('id', 'vehicletype as name')->where('status',1)->orderBy('vehicletype', 'ASC')->get();
                break;
            case 'Designation of Member':
                $data = Designation::select('id', 'designationname as name')->where('status',1)->orderBy('designationname', 'ASC')->get();
                break;
            case 'Dealer':
                $data = Dealer::select('id', 'dealername as name')->where('status',1)->orderBy('dealername', 'ASC')->get();
                break;
            case 'State':
                if ($session['admintype'] == 0) {
                    $data = State::select('id', 'statename as name')->where('status',1)->orderBy('statename', 'ASC')->get();
                }else{
                    $data = State::select('id', 'statename as name')->where('status',1)->where('id',$session['admintype'])->orderBy('statename', 'ASC')->get(); 
                }
                break;
            case 'City':
                if ($session['admintype'] == 0) {
                    $data = City::select('id', 'cityname as name')->where('status',1)->orderBy('cityname', 'ASC')->get();
                }else{
                    $data = City::select('id', 'cityname as name')->where('status',1)->where('stateid',$session['admintype'])->orderBy('cityname', 'ASC')->get(); 
                }
                
                break;
            case 'Office Bearers':
                $data = Officebearer::select('id', 'bearername as name')->where('status',1)->orderBy('bearername', 'ASC')->get();
                
                break;
            default:
                $data = FALSE;
        }
        return $data;
    }

    public function selectMembers(Request $request){
        $session = $request->session()->get('fada_admin');
        $data = $request->all();
        
        if(count($data) == 1){
            //$data = User::where('status',1)->get();
            $data = DB::table('users')
            ->select(
                'users.id',
                'users.username',
                'users.title',
                'users.firstname',
                'users.lastname',
                'users.email',
                'users.mobile',
                'manufacturers.manufacturername as manufacturer',
                'dealers.dealername as dealer',
                'designations.designationname as designation',
                'users.address1',
                'users.address2',
                'cities.cityname as city',
                'districts.districtname as district',
                'states.statename as state',
                'users.pin',
                'users.type'
            )
            ->join(
                'manufacturers',
                'manufacturers.id','=','users.manufacturerid'
            )
            ->join(
                'dealers',
                'dealers.id','=','users.dealerid'
            )
            ->join(
                'designations',
                'designations.id','=','users.designationid'
            )
            ->join(
                'cities',
                'cities.id','=','users.cityid'
            )
            ->join(
                'districts',
                'districts.id','=','users.districtid'
            )
            ->join(
                'states',
                'states.id','=','users.stateid'
            )
            ->where(['users.status' => 1]);
            if($session['admintype'] != 0){
                $data = $data->where('users.stateid',$session['admintype']);
            }
            $data = $data->get();
        }else{
            unset($data['_token']);
            
            $filter = array();
            foreach($data as $key =>$value){
                $temp = Filter::find($key);
                switch ($temp->filtertable) {
                    case 'Manufacturer':
                        $filter['manufacturerid'] = $value;
                        break;
                    case 'Vehicle':
                        $filter['vehicletypeid'] = $value;
                        break;
                    case 'Designation':
                        $filter['designationid'] = $value;
                        break;
                    case 'Dealer':
                        $filter['dealerid'] = $value;
                        break;
                    case 'State':
                        $filter['stateid'] = $value;
                        break;
                    case 'City':
                        $filter['cityid'] = $value;
                        break;
                    case 'Officebearers':
                        $filter['is_officebearer'] = $value;
                        break;
                }
                if(array_key_exists("is_officebearer", $filter) && !array_key_exists("stateid", $filter)){
                    $filter['is_national'] = ["1"];
                }elseif(array_key_exists("is_officebearer", $filter) && array_key_exists("stateid", $filter)){
                    $filter['is_national'] = ["0"];
                }

            }
            $data = DB::table('users')
            ->select(
                'users.id',
                'users.username',
                'users.title',
                'users.firstname',
                'users.lastname',
                'users.email',
                'users.mobile',
                'manufacturers.manufacturername as manufacturer',
                'dealers.dealername as dealer',
                'designations.designationname as designation',
                'users.address1',
                'users.address2',
                'cities.cityname as city',
                'districts.districtname as district',
                'states.statename as state',
                'users.pin',
                'users.type'
            )
            ->join(
                'manufacturers',
                'manufacturers.id','=','users.manufacturerid'
            )
            ->join(
                'dealers',
                'dealers.id','=','users.dealerid'
            )
            ->join(
                'designations',
                'designations.id','=','users.designationid'
            )
            ->join(
                'cities',
                'cities.id','=','users.cityid'
            )
            ->join(
                'districts',
                'districts.id','=','users.districtid'
            )
            ->join(
                'states',
                'states.id','=','users.stateid'
            )
            ->where(['users.status' => 1]);
            foreach($filter as $key => $value){
                $data->whereIn('users.'.$key, $value);
            }
            if($session['admintype'] != 0){
                $data = $data->where('users.stateid',$session['admintype']);
            }
            $data = $data->get();
        }
        $array = array('users'=>$data);
        return View::make('viewGroupMembers')->with($array);
    }

    public function createGroup(Request $request){
        $validator = Validator::make($request->all(), [
            'users' => 'required',
            'groupname' => 'required',
            'grouplogo' => 'required',
        ],[
            'required' => 'The :attribute is required.',
        ]);
        if ($validator->fails()) {
            Session::flash('group-error', 'Cannot Create New Group'); 
            return redirect('creategroup');
        }
        $filepath = NULL;
        if($request->hasfile('grouplogo')) 
        { 
            $file = $request->file('grouplogo');
            $extension = $file->getClientOriginalExtension();
            $filename =str_random(8).'_'.time().'.'.$extension;
            $destinationPath = storage_path('uploads/logos/');
            $filestatus = $file->move($destinationPath, $filename);
            $filepath = url('storage/uploads/logos/').'/'.$filename;
        }

        $data = new Group;
        $data->icon = $filepath;
        $data->name = $request->input('groupname');
        $data->status = 1; 
        $data->channelurl = ""; 
        $users = explode(',',$request->input('users'));
        $users = array_filter($users, 'strlen');
        $data->save();
        if($data->id){
            $i=0;
            $datas= array();
            $userdata = array();
            foreach($users as $key => $value){
                $datas[$i]['groupid'] = $data->id;
                $datas[$i]['userid'] = $value;
                $userdata[] = "fada_user_".$value;
                $datas[$i]['status'] = 1;
                $i++; 

            }
            $response = Groupuser::insert($datas);
           // return $response;
            if($response){
                $post = [
                    'name' => $data->name,
                    'cover_url' => $data->icon,
                    "custom_type"=> "personal",
                    "data"=> "",
                    "user_ids" => $userdata
                ];
                
                $curl = curl_init();
                
                curl_setopt_array($curl, array(
                    CURLOPT_URL => "https://api.sendbird.com/v3/group_channels",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30000,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => json_encode($post),
                    CURLOPT_HTTPHEADER => array(
                        "Content-Type: application/json, charset=utf8",
                        "Api-Token: 0b999a94f8d57e44283235257a18c96855072d9c"
                    ),
                ));
                
                $response = curl_exec($curl);
                $err = curl_error($curl);
                $response = json_decode($response);
                $data->channelurl = $response->channel_url;
                $data->save();
                curl_close($curl);
                Session::flash('group-success', 'New Group Successfully Created'); 
                return redirect('viewgroups'); 
            }else{
                Session::flash('group-error', 'Cannot Create Group Users'); 
                return redirect('viewgroups');
            }
        }else{
            Session::flash('group-error', 'Cannot Create New Group'); 
            return redirect('creategroup');   
        }
    }

    public function showGroups(Request $request){

        $sidebar = array('section'=>'grp','activity'=>'view');
        Session::flash('admin-success', $sidebar);

        $session = $request->session()->get('fada_admin');
        $groups = Group::all();
        $i=0;

        foreach($groups as $group){
            $groups[$i]['users'] = DB::table('groupusers')
            ->select(
                'groupusers.id as groupuserid',
                'groupusers.status as groupuserstatus',
                'users.id as userid',
                'users.title as title',
                'users.firstname as firstname',
                'users.lastname as lastname'
            )
            ->join(
                'users',
                'users.id','=','groupusers.userid'
            )
            ->where(['groupusers.groupid' => $group->id])
            ->where(['users.status' => 1]);
            if($session['admintype'] != 0){
                $groups[$i]['users'] = $groups[$i]['users']->where('users.stateid',$session['admintype']);
            }
            $groups[$i]['users'] = $groups[$i]['users']->get(); 
            $i++;
        }
        $array = array('groups'=>$groups);
        return View::make('showGroups')->with($array);
                
    }

    public function changegroupmemberStatus(Request $request){
        $validator = Validator::make($request->all(), [
            'groupuserid' => 'required',
            'groupid' => 'required',
        ],[
            'required' => 'The :attribute is required.',
        ]);
        if ($validator->fails()) {
            return 2;
        }
        $user = Groupuser::find($request->input('groupuserid'));
        $group = Group::find($request->input('groupid'));
        switch($user->status) {
            case 0:
                $user->status = 1;
                $update = $user->save();
                if($update){
                    $url = "https://api.sendbird.com/v3/group_channels/".$group->channelurl."/ban/fada_user_".$user->userid;
                    $curl = curl_init();
                    $post = array();
                    curl_setopt_array($curl, array(
                        CURLOPT_URL => $url,
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 30000,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "DELETE",
                        CURLOPT_POSTFIELDS => json_encode($post),
                        CURLOPT_HTTPHEADER => array(
                            "Content-Type: application/json, charset=utf8",
                            "Api-Token: 0b999a94f8d57e44283235257a18c96855072d9c"
                        ),
                    ));
                    
                    $response = curl_exec($curl);
                    $err = curl_error($curl);
                    curl_close($curl);
                    return $user->status;
                }else{
                    return 2;
                }
                break;
            case 1:
                $user->status = 0;
                $update = $user->save();
                if($update){
                    $url = "https://api.sendbird.com/v3/group_channels/".$group->channelurl."/ban";
                    $post = [
                        'user_id' => "fada_user_".$user->userid
                    ];
                    $curl = curl_init();
                    
                    curl_setopt_array($curl, array(
                        CURLOPT_URL => $url,
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 30000,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "POST",
                        CURLOPT_POSTFIELDS => json_encode($post),
                        CURLOPT_HTTPHEADER => array(
                            "Content-Type: application/json, charset=utf8",
                            "Api-Token: 0b999a94f8d57e44283235257a18c96855072d9c"
                        ),
                    ));
                    
                    $response = curl_exec($curl);
                    $err = curl_error($curl);
                    curl_close($curl);
                    return $user->status;
                }else{
                    return 2;
                }
               break;
            }
        
       
        
    }

    public function changegroupStatus(Request $request){
        $validator = Validator::make($request->all(), [
            'groupid' => 'required',
        ],[
            'required' => 'The :attribute is required.',
        ]);
        if ($validator->fails()) {
            return 2;
        }
        $group = Group::find($request->input('groupid'));
        switch($group->status) {
            case 0:
                $post = [
                    'freeze' => "FALSE"
                ];
               $group->status = 1;
               break;
            case 1:
            $post = [
                'freeze' => true
            ];
                $group->status = 0;
               break;
            }
        $update = $group->save();
        if($update){
            
            $url = "https://api.sendbird.com/v3/group_channels/".$group->channelurl."/freeze";
            $curl = curl_init();
            
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "PUT",
                CURLOPT_POSTFIELDS => json_encode($post),
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json, charset=utf8",
                    "Api-Token: 0b999a94f8d57e44283235257a18c96855072d9c"
                ),
            ));
            
            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            return $group->status;
        }else{
            return 2;
        }
        
    }

    public function changegroupName(Request $request){
        $validator = Validator::make($request->all(), [
            'groupid' => 'required',
            'groupname' => 'required',
        ],[
            'required' => 'The :attribute is required.',
        ]);
        if ($validator->fails()) {
            return 0;
        }
        
        $group = Group::find($request->input('groupid'));
        $group->name = $request->input('groupname');
        $update = $group->save();
        if($update){
            $post = [
                'name' => $request->input('groupname')
            ];
            $url = "https://api.sendbird.com/v3/group_channels/".$group->channelurl;
            $curl = curl_init();
            
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "PUT",
                CURLOPT_POSTFIELDS => json_encode($post),
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json, charset=utf8",
                    "Api-Token: 0b999a94f8d57e44283235257a18c96855072d9c"
                ),
            ));
            
            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            return 1;
        }else{
            return 0;
        }
    }

    public function changegroupIcon(Request $request){
        $validator = Validator::make($request->all(), [
            'groupid' => 'required',
            'grouplogo' => 'required',
        ],[
            'required' => 'The :attribute is required.',
        ]);
        if ($validator->fails()) {
            return 0;
        }
       
        if($request->hasfile('grouplogo')) 
        { 
            $file = $request->file('grouplogo');
            $extension = $file->getClientOriginalExtension();
            $filename =str_random(8).'_'.time().'.'.$extension;
            $destinationPath = storage_path('uploads/logos/');
            $filestatus = $file->move($destinationPath, $filename);
            $filepath = url('storage/uploads/logos/').'/'.$filename;
        }
        $group = Group::find($request->input('groupid'));
        $group->icon = $filepath;
        $update = $group->save();
        if($update){
            $post = [
                'cover_url ' => $filepath
            ];
            $url = "https://api.sendbird.com/v3/group_channels/".$group->channelurl;
            $curl = curl_init();
            
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "PUT",
                CURLOPT_POSTFIELDS => json_encode($post),
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json, charset=utf8",
                    "Api-Token: 0b999a94f8d57e44283235257a18c96855072d9c"
                ),
            ));
            
            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            return $filepath;
        }else{
            return 0;
        }
    }

    public function deleteGroup(Request $request){
        $validator = Validator::make($request->all(), [
            'groupid' => 'required',
        ],[
            'required' => 'The :attribute is required.',
        ]);
        if ($validator->fails()) {
            return 0;
        }
        $data = Group::find($request->input('groupid'));
        $group = $data->delete();
        if($group){
            $url = "https://api.sendbird.com/v3/group_channels/".$data->channelurl;
            $curl = curl_init();
            $post = array();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "DELETE",
                CURLOPT_POSTFIELDS => json_encode($post),
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json, charset=utf8",
                    "Api-Token: 0b999a94f8d57e44283235257a18c96855072d9c"
                ),
            ));
            
            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            $groupuser =Groupuser::where('groupid',$request->input('groupid'))->delete();
            return 1;
        }
        return 0;
    }

    public function getMembers(Request $request){
        $session = $request->session()->get('fada_admin');
        $grpuser = array();
        $group = Group::find($request->id);
        if(!$group){
            Session::flash('group-error', 'Selected group is not valid');
            return redirect('viewgroups');
        }
        $groupusers =Groupuser::where('groupid',$request->id)->get();
        foreach($groupusers as $g){
            $grpuser[]= $g->userid;
        }
        $data = DB::table('users')
            ->select(
                'users.id',
                'users.username',
                'users.title',
                'users.firstname',
                'users.lastname',
                'users.email',
                'users.mobile',
                'manufacturers.manufacturername as manufacturer',
                'vehicles.vehicletype',
                'dealers.dealername as dealer',
                'designations.designationname as designation',
                'users.address1',
                'users.address2',
                'cities.cityname as city',
                'districts.districtname as district',
                'states.statename as state',
                'users.pin',
                'users.type'
            )
            ->join(
                'manufacturers',
                'manufacturers.id','=','users.manufacturerid'
            )
            ->join(
                'dealers',
                'dealers.id','=','users.dealerid'
            )
            ->join(
                'vehicles',
                'vehicles.id','=','users.vehicletypeid'
            )
            ->join(
                'designations',
                'designations.id','=','users.designationid'
            )
            ->join(
                'cities',
                'cities.id','=','users.cityid'
            )
            ->join(
                'districts',
                'districts.id','=','users.districtid'
            )
            ->join(
                'states',
                'states.id','=','users.stateid'
            )
            ->where(['users.status' => 1])
            ->whereNotIn('users.id', $grpuser);
            if($session['admintype'] != 0){
                $data = $data->where('users.stateid',$session['admintype']);
            }
            $data = $data->get();
        $array = array('users'=>$data,'groupid'=>$request->id,'groupname'=>$group->name,'groupicon'=>$group->icon);
        return View::make('addMembers')->with($array);

    }

    public function addMembers(Request $request){
        $validator = Validator::make($request->all(), [
            'users' => 'required',
            'groupid' => 'required'
        ], [
            'required' => 'The :attribute is required.',
        ]);
        if ($validator->fails()) {
            Session::flash('group-error', 'Please select members');
            return redirect('addmembers/'.$request->input('groupid'));
        }
        $group = Group::find($request->input('groupid'));
        if(!$group){
            Session::flash('group-error', 'Selected group is not valid');
            return redirect('viewgroups');
        }
        $users = explode(',',$request->input('users'));
        $users = array_filter($users, 'strlen');
        $datas= array();
        $userdata = array();
        $i=0;
        foreach($users as $key => $value){
            $datas[$i]['groupid'] = $request->input('groupid');
            $datas[$i]['userid'] = $value;
            $userdata[] = "fada_user_".$value;
            $datas[$i]['status'] = 1;
            $i++; 

        }
        $response = Groupuser::insert($datas);
        if($response){
            $url = "https://api.sendbird.com/v3/group_channels/".$group->channelurl."/invite";
            $post = [
                "user_ids" => $userdata
            ];
            $curl = curl_init();
            
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($post),
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json, charset=utf8",
                    "Api-Token: 0b999a94f8d57e44283235257a18c96855072d9c"
                ),
            ));
            
            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            Session::flash('group-success', 'New member added successfully');
            return redirect('addmembers/'.$request->input('groupid'));
        }else{
            Session::flash('group-error', 'Members cannot be added. Please try again');
            return redirect('addmembers/'.$request->input('groupid')); 
        }
    }
}
