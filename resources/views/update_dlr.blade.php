@extends('AdminLayout.admin_frame')

@section('content')


<!--main-container-part-->

<div id="content">

  <!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
  </div>
  <!--End-breadcrumbs-->
    
  
<div  class="container-fluid">

  <hr>
  <div class="row-fluid">
    <div class="span6">

      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Edit Dealer</h5>
        </div>
        <div class="widget-content nopadding">
          <form action="update_dlr" method="post" class="form-horizontal">
          {{ csrf_field() }}
            <div class="control-group">
              <label class="control-label">Dealer Name :</label>
              <div class="controls">
                <input id="dlr_id"style="display:none;" name="dlr_id" class="span11"  />
                <input name="dlr_name" id="dlr_name" type="text" class="span11 required" placeholder="Name" />
              </div>
            </div>
            <div class="form-actions">
              <input id="dlr_button" type="submit" class="btn btn-success pull-right" value="Save" />
            </div>
          </form>
        </div>
      </div>
      <p  style="color:green;width:100%;text-align:center;" ><?php if(!empty($data['fail'])){ echo $data['fail']; } ?></p>
      <p   style="color:green;width:100%;text-align:center;" ><?php if(!empty($data['success'])){ echo $data['success']; } ?></p>
    </div>
  </div>
</div>  

</div>

<!--end-main-container-part-->


<script>
 

 var dlr = <?php if(!empty($data)){ echo json_encode($data); } ?>;

 var name = dlr.dlr_name;
 var id = dlr.id;

 $(document).ready(function(){
     
     $('#dlr_name').val(name);
     $('#dlr_id').val(id);
   
 });




</script>

@endsection