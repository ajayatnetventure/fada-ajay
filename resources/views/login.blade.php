<?php $type = 0; 
if($errors == '[["Email is required for setting new password."]]' || $errors == '[["Invalid Email Address."]]' || $errors == '[["Cannot create new password. Please try again."]]'){
    $type = 1;
}
?>

<!DOCTYPE html>
<html lang="en">
    
    <head>
        <title>Matrix Admin</title><meta charset="UTF-8" />
        
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="stylesheet" href="public/css/backend_css/bootstrap.min.css" />
		<link rel="stylesheet" href="public/css/backend_css/bootstrap-responsive.min.css" />
        <link rel="stylesheet" href="public/css/backend_css/matrix-login.css" />
        <link href="public/font-awesome/css/font-awesome.css" rel="stylesheet" />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
        
    </head>
    <body>
        <div id="loginbox">            
            <form id="loginform" class="form-vertical" method="post" action="./login" <?php if($type){echo 'style="display:none;"';} ?>>
            {{ csrf_field() }}
                 
				 <div class="control-group normal_text"> <h3>Administrator</h3></div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_lg"><i class="icon-user"> </i></span><input id="username" type="text" name="username" placeholder="Username" class="required"/>
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_ly"><i class="icon-lock"></i></span><input id="password" type="password" name="password" placeholder="Password" class="required"/>
                        </div>
                    </div>
                </div>
                @if(count($errors) != 0 && !$type) 
                <div class="form-actions">
                    <p style="font-size: 14px;color: red;text-align: center;" class="has-error">{{str_replace(array("[","]",'"'),' ',$errors)}}</p>
                </div>
                @endif
                @if (Session::has('passwordreset'))
                <div class="form-actions">
                    <p style="font-size: 14px;color: #10ff00;text-align: center;" class="has-success">{!! session('passwordreset') !!}</p>
                </div>
                @endif
                <div class="form-actions">
                    <span class="pull-left"><a href="#" class="flip-link btn btn-info" id="to-recover">Lost password?</a></span>
                    <span class="pull-right"><input id="loginformSubmit" type="submit" value="login"  class="btn btn-success" /></span>
                </div>
                
            </form>
            <form id="recoverform" action="./forgotpassword" class="form-vertical" method="post" <?php if($type){echo 'style="display:block;"';} ?>>
                {{ csrf_field() }}
				<p class="normal_text">Enter your e-mail address below and we will send you instructions how to recover a password.</p>
				
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_lo"><i class="icon-envelope"></i></span><input class="required" type="text" id="email" name="email" placeholder="E-mail address" />
                        </div>
                    </div>
                    @if(count($errors) != 0 && $type) 
                    <div class="form-actions">
                        <p style="font-size: 14px;color: red;text-align: center;" class="has-error">{{str_replace(array("[","]",'"'),' ',$errors)}}</p>
                    </div>
                    @endif
                <div class="form-actions">
                    <span class="pull-left"><a href="#" class="flip-link btn btn-success" id="to-login">&laquo; Back to login</a></span>
                    <span class="pull-right"><input id="recoverformSubmit" type="submit" value="Recover"  class="btn btn-info" /></span>
                </div>
                
            </form>
        </div>
        
        <script src="public/js/backend_js/jquery.min.js"></script>  
        <script src="public/js/backend_js/matrix.login.js"></script> 
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
        <script>
            function validate_email(v) {
                var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                return emailReg.test( v );
            }
            $(document).ready(function () {
                $('#loginform').validate({ 
                    rules: {
                        username: {
                            required: true
                        },
                        password: {
                            required: true
                        }
                    },
                    errorPlacement: function(error, element) {
                        if (element.attr("name") == "username" ) {
                            $('#username').css("border", "2px solid rgb(255, 0, 0)");
                        }
                        else if (element.attr("name") == "password" ) {
                            $('#password').css("border", "2px solid rgb(255, 0, 0)");
                        }
                    }
                });

                $( ".required" ).on("change", function() {
                    var v = $(this).val();
                    var key = $(this).attr("name");
                    var type = $(this).attr("type");
                    if((v.replace(/\s+/g, '')) == '') {
                        access = false;
                        $(this).css("border", "2px solid rgb(255, 0, 0)");
                    }else {
			            $(this).css('border', '');
                        if(key == "email") {
                            if(!validate_email(v)) {
                                access = false;
                                $(this).css("border", "2px solid rgb(255, 0, 0)");
                            }
                        }
                    }
                });

                $('#recoverform').validate({ 
                    rules: {
                        email: {
                            required: true,
                            email: true
                        }
                    },
                    errorPlacement: function(error, element) {
                        if (element.attr("name") == "email" ) {
                            $('#email').css("border", "2px solid rgb(255, 0, 0)");
                        }
                        
                    }
                });
            });
        </script>
    </body>

</html>


