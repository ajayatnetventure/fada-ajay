@extends('AdminLayout.admin_frame')

@section('content')


<!--main-container-part-->

<div id="content">

  <!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
  </div>
  <!--End-breadcrumbs-->
    
  
<div  class="container-fluid">

  <hr>
  <div class="row-fluid">
    <div class="span6">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Edit Designation</h5>
        </div>
        <div class="widget-content nopadding">
          <form id="dsgformupdate" action="update_dsg" method="post" class="form-horizontal">
          {{ csrf_field() }}
            <div class="control-group">
              <label class="control-label">Dealer Name :</label>
              <div class="controls">
                <input id="dsg_id"style="display:none;" name="dsg_id" class="span11"  />
                <input name="dsg_name" id="dsg_name" type="text" class="span11 required" placeholder="Name" />
              </div>
            </div>
            <div class="form-actions">
              <input id="dsg_button" type="submit" class="btn btn-success pull-right" value="Save" />
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <p style="color:green;width:50%;text-align:center;" ><?php if(!empty($req['message'])){ echo $req['message']; } ?></p>
</div>  

</div>

<!--end-main-container-part-->
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>
  var dsg = <?php if(!empty($req)){ echo json_encode($req); } ?>;

 var name = dsg.dsg_name;
 var id = dsg.id;

 $(document).ready(function(){
     
     $('#dsg_name').val(name);
     $('#dsg_id').val(id);
     
     $('#dsgformupdate').validate({ 
        rules: {
          dsg_name: {
                required: true
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "dsg_name" ) {
                $('#dsg_name').css("border", "2px solid rgb(255, 0, 0)");
            }
        }

    }); 
    $( ".required" ).on("change", function() {
        var v = $(this).val();
        if((v.replace(/\s+/g, '')) == '') {
          access = false;
          $(this).css("border", "2px solid rgb(255, 0, 0)");
        }else {
          $(this).css('border', '');
        }
      }); 
   
 });
</script>




@endsection