@extends('AdminLayout.admin_frame')

@section('content')

<style>

.custom_user_detalis{ background-color:;margin-top:5px; }

</style>

<!--main-container-part-->
<div id="content">

  <!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{url('dashboard')}}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
  </div>
  <!--End-breadcrumbs-->
    
  <div  class="container-fluid">

    <hr>

    <div class="row-fluid">
      <div class="span6">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
            <h5>User Details</h5>
          </div>
          <div class="widget-content nopadding">
            <div class="form-horizontal" style="padding:20px;" >

              <div class="control-group" >
                <label class="control-label">Admin Username :</label>
                <div class="controls">
                  <div class="span11 custom_user_detalis"  >
                  {{$data['ad_user_name']}}
                  </div>
                </div>
              </div>
              <div class="control-group" >
                <label class="control-label">Admin Email :</label>
                <div class="controls">
                  <div class="span11 custom_user_detalis"  >
                  {{$data['ad_email']}}
                  </div>
                </div>
              </div>  
              <div class="control-group" >
                <label class="control-label">Admin Type :</label>
                <div class="controls">
                  <div class="span11 custom_user_detalis"  >
                  {{$data['ad_type']}}
                  </div>
                </div>
              </div>

              @if(!empty($data['ad_state']))
                <div class="control-group" >
                  <label class="control-label">State :</label>
                  <div class="controls">
                    <div class="span11 custom_user_detalis"  >
                    {{$data['ad_state']}}
                    </div>
                  </div>
                </div>
              @endif    

             </div>


            </div>  
          </div>
        </div>
      </div>
    </div> 

  </div>

</div>
<!--main-container-end-->


@endsection