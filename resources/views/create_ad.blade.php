@extends('AdminLayout.admin_frame')

@section('content')


<!--main-container-part-->

<div id="content">

  <!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{url('dashboard')}}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
  </div>
  <!--End-breadcrumbs-->
    
  
<div  class="container-fluid">

  <hr>
  <div class="row-fluid">
    <div class="span6">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Create A New Admin</h5>
        </div>
        <div class="widget-content nopadding">
          <form  id="create_admin_form" name="create_admin_form" action="create_ad" method="post" class="form-horizontal">
          {{ csrf_field() }}
            <div class="control-group">
              <label class="control-label">Admin Username :</label>
              <div class="controls">
                <input name="ad_name" id="ad_name" type="text" class="span11 required" placeholder="Username" />
                <p style="color:red;text-align:left;" ><?php if(!empty($data['FailMsg_ad_name'])){ echo $data['FailMsg_ad_name']; } ?></p>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Admin email :</label>
              <div class="controls">
                <input name="ad_email" id="ad_email" type="email" class="span11 required" placeholder="Email" />
                <p style="color:red;text-align:left;" ><?php if(!empty($data['FailMsg_ad_email'])){ echo $data['FailMsg_ad_email']; } ?></p>
              </div>
            </div>            
            <div class="control-group">
              <label class="control-label">Admin Type :</label>              
              <div class="controls">
                <select id="ad_type" name="ad_type" class="required" >
                    <option value="" ><div >Select Admin Type</div></option>';         
                    <option value="0" ><div >Super Admin</div></option>';
                    <option value="1" ><div >State Admin</div></option>';
                </select> 
                <p style="color:red;text-align:left;" ><?php if(!empty($data['FailMsg_ad_type'])){ echo $data['FailMsg_ad_type']; } ?></p>
              </div>
            </div>
            <div class="control-group" id="state_container"  style="display:none;" >
              <label class="control-label">State :</label>
              <div class="controls">
                <select id="state" name="state" class="required" >
                  <option value="" ><div >Select states</div></option>';         
                  <?php
                  if(!empty($data['states']))
                  {
                     foreach($data['states'] as $state)
                     {
                      echo '<option value="'.$state['id'].'"><div >'.$state['statename'].'</div></option>';
                     }
                  }   
                  ?>
                </select> 
              </div>
            </div>            
            <div class="form-actions">
              <input id="ad_button" type="submit" class="btn btn-success pull-right" value="Save" />
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <p style="color:green;width:50%;text-align:center;" ><?php if(!empty($data['success_msg'])){ echo $data['success_msg']; } ?></p>
</div>  

</div>

<!--end-main-container-part-->
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>

<script>
  
  var ad_type; 
  $("#ad_type").click(function(){

    ad_type = $('#ad_type').val(); 

    if(ad_type == 1) 
    {
      $('#state_container').show();
    }
    else{
      $('#state_container').hide();
    }

    
  });
  $(document).ready(function () {
  $('#create_admin_form').validate({ 
        rules: {
          ad_name: {
                required: true
            },
            ad_email: {
                required: true
            },
            ad_type: {
                required: true
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "ad_name" ) {
                $('#ad_name').css("border", "2px solid rgb(255, 0, 0)");
            }
            if (element.attr("name") == "ad_email" ) {
                $('#ad_email').css("border", "2px solid rgb(255, 0, 0)");
            }
            if (element.attr("name") == "ad_type" ) {
                $('#ad_type').css("border", "2px solid rgb(255, 0, 0)");
            }
        }

    }); 
    function validate_email(v) {
      var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
      return emailReg.test( v );
    }
    $( ".required" ).on("change", function() {
      var v = $(this).val();
      var type = $(this).attr('type');
      if((v.replace(/\s+/g, '')) == '') {
        access = false;
        $(this).css("border", "2px solid rgb(255, 0, 0)");
      }else {
        $(this).css('border', '');
        if(type == "email") {
          if(!validate_email(v)) {
            access = false;
            $(this).css("border", "2px solid rgb(255, 0, 0)");
          }
        }
      }
    });    
  });

</script>

@endsection