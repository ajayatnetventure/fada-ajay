@extends('AdminLayout.admin_frame')

@section('content')


<!--main-container-part-->

<div id="content">

  <!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{url('dashboard')}}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
  </div>
  <!--End-breadcrumbs-->
    
  
    <div  class="container-fluid">
    <hr>
    <div class="row-fluid">
        <div class="span6">
            <div class="widget-box">
                <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                    <h5>Create A New vehicle</h5>
                </div>
                <div class="widget-content nopadding">
                <form id="vehicleform" action="create_veh" method="post" class="form-horizontal">
                {{ csrf_field() }}
                    <div class="control-group">
                        <label class="control-label">Vehicle Name :</label>
                        <div class="controls">
                            <input name="veh_name" id="veh_name" type="text" class="span11 required" placeholder="Name" />
                        </div>
                    </div>
                    <div class="form-actions">
                    <input id="veh_button" type="submit" class="btn btn-success pull-right" value="Save" />
                    </div>
                </form>
                </div>
            </div>
            <p  style="color:green;width:100%;text-align:center;" ><?php if(!empty($data['fail'])){ echo $data['fail']; } ?></p>
            <p   style="color:green;width:100%;text-align:center;" ><?php if(!empty($data['success'])){ echo $data['success']; } ?></p>
        </div>
    </div>
    </div>  

</div>

<!--end-main-container-part-->
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>
$(document).ready(function () {
  $('#vehicleform').validate({ 
        rules: {
            veh_name: {
                required: true
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "veh_name" ) {
                $('#veh_name').css("border", "2px solid rgb(255, 0, 0)");
            }
        }

    }); 
  $( ".required" ).on("change", function() {
      var v = $(this).val();
      if((v.replace(/\s+/g, '')) == '') {
        access = false;
        $(this).css("border", "2px solid rgb(255, 0, 0)");
      }else {
        $(this).css('border', '');
      }
    });    
  });
</script>


@endsection