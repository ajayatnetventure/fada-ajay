@extends('AdminLayout.admin_frame')

@section('content')

<!--main-container-part-->

<div id="content">

  <!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{url('dashboard')}}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
  </div>
  <!--End-breadcrumbs-->
    
  <div class="container-fluid">
    <hr>
    @if(Session::has('member-success'))
    <p style="color:green;text-align:center;font-size: 13px;" >
        {{ Session::get('member-success') }}
    </p>
    @endif
    @if(Session::has('member-error'))
    <p style="color:red;text-align:center;font-size: 13px;" >
    {{ Session::get('member-error') }}
    </p>
    @endif
    
    <form id="officebearerselectform" action="" method="post" class="form-horizontal">
      <div class="row-fluid">
        <div class="span6">
          <div class="widget-box">
            <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
              <h5>Office Bearer Title</h5>
            </div>
            <div class="widget-content nopadding">
              <div class="control-group">
                <div class="controls">
                    <select id="bearer_title" name="bearer_title" class="required" >
                        <option value="" >Select Office Bearer Title</option>;         
                        <option value="1" >President</option>;
                        <option value="2" >Vice President</option>;
                        <option value="3" >Secretary</option>;
                        <option value="4" >Treasurer</option>;
                    </select> 
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="span6">
          <div class="widget-box">
            <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
              <h5>National / State</h5>
            </div>
            <div class="widget-content nopadding">
              <div class="control-group">
                <div class="controls">
                    <select id="bearer_type" name="bearer_type" class="required" >
                        <option value="" >Select National / State</option>;         
                        <option value="1" >National</option>;
                        <option value="0" >State</option>;
                    </select> 
                </div>
              </div>
            </div>
          </div>
          <div id="statebox" class="widget-box" style="display:none;">
            <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
              <h5>Select State</h5>
            </div>
            <div class="widget-content nopadding">
              <div class="control-group">
                <div class="controls">
                    <select id="state" name="state" class="required" >
                        <option value="" >Select State</option>;
                        @foreach($states as $state)         
                          <option value="{{$state->id}}" >{{$state->statename}}</option>;
                        @endforeach
                    </select> 
                </div>
              </div>
            </div>
          </div>
        </div>
        
      </div>
      <div class="row-fluid">
        <div class="span12">
          <div class="widget-box">
            <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
              <h5>Select Member</h5>
            </div>
            <div class="widget-content nopadding">
              <table class="table table-bordered data-table">
                <thead>
                  <tr>
                    <th></th>
                    <th>Name</th>
                    <th>Designation</th>
                    <th>Company</th>
                    <th>Dealer of</th>
                    <th>Address</th>
                    <th>Email</th>
                    <th>Mobile</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($users as $user)
                  <tr id="user-{{$user->id}}" data-id="{{$user->id}}" data-stateid="{{$user->stateid}}">
                    <td><input id="member-checkbox-{{$user->id}}" type="radio"  class="member-checkbox" onclick="selectmember({{$user->id}})"/></td>
                    <td>{{$user->title}}. {{$user->firstname}} {{$user->lastname}}</td>
                    <td>{{$user->designation}}</td>
                    <td>{{$user->dealer}}</td>
                    <td>{{$user->manufacturer}}</td>
                    <td>{{$user->address1}}, {{$user->address2}},<br>{{$user->city}},<br>{{$user->state}},<br>PIN: {{$user->pin}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->mobile}}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>   
        </div>
      </div>
      <div class="row-fluid">
        <div class="span12">
          <div class="widget-box">
            {{ csrf_field() }}
            <input id="memberselectinput" name="user" type="hidden" value="">
            <div class="form-actions">
              <input id="createbutton" type="submit" class="btn btn-info pull-right" value="Add Office Bearer" />
            </div>   
          </div>
        </div>
      </div>
    </form>
  </div>

</div>
<!--end-main-container-part-->
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>

  $(document).ready(function(){
	
    $('.data-table').dataTable({
      "bJQueryUI": true,
      "sPaginationType": "full_numbers",
      "sDom": '<""l>t<"F"fp>'
    });
    $('#officebearerselectform').validate({ 
      ignore: [],
      rules: {
          user: {
              required: true
          },
          bearer_title: {
              required: true
          },
          bearer_type: {
              required: true
          },
          state: {
              required: function(element){
                  return $("#bearer_type").val() == 0;
              }
          }
      },
      errorPlacement: function(error, element) {
          
          if (element.attr("name") == "user" ) {
            $('#member-error').remove();
            $('#createbutton').after('<p id="member-error" style="color:red;font-size: 13px;">Please select a member.</p>');
          }
          if (element.attr("name") == "bearer_title" ) {
                $('#bearer_title').css("border", "2px solid rgb(255, 0, 0)");
            }
          if (element.attr("name") == "bearer_type" ) {
              $('#bearer_type').css("border", "2px solid rgb(255, 0, 0)");
          }
          if (element.attr("name") == "state" ) {
              $('#state').css("border", "2px solid rgb(255, 0, 0)");
          }
      }
    }); 
    $( ".required" ).on("change", function() {
      var v = $(this).val();
      if((v.replace(/\s+/g, '')) == '') {
        access = false;
        $(this).css("border", "2px solid rgb(255, 0, 0)");
      }else {
        $(this).css('border', '');
      }
    }); 
    $( "#bearer_type" ).on("change", function() {
      var val = $(this).val();
      var table = $('.data-table').DataTable();
      $.fn.dataTable.ext.search.pop();
      table.draw();
      if(val == 1 || !val){
        $('#statebox').hide();
      }else{
        $('#statebox').show();
      }
    });
    $( "#state" ).on("change", function() {
      var table = $('.data-table').DataTable();
      var state = $(this).val();
      $.fn.dataTable.ext.search.pop();
      $.fn.dataTable.ext.search.push(
      function(settings, data, dataIndex) {
        return $(table.row(dataIndex).node()).attr('data-stateid') == state;
      });
      table.draw();
    });
   
  });
    function selectmember(userid){
        $('.member-checkbox').prop('checked', false);
        $('#member-checkbox-'+userid).prop('checked', true);
        $('#memberselectinput').val(userid);
        $('#member-error').remove();
    }

</script>



@endsection