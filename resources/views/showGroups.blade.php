@extends('AdminLayout.admin_frame')

@section('content')

<!--main-container-part-->

<div id="content">

  <!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
  </div>
  <!--End-breadcrumbs-->
    
  <div class="container-fluid">
    <hr>
    @if(Session::has('group-success'))
    <p style="color:green;text-align:center;font-size: 13px;" >
        {{ Session::get('group-success') }}
    </p>
    @endif
    @if(Session::has('group-error'))
    <p style="color:red;text-align:center;font-size: 13px;" >
    {{ Session::get('group-error') }}
    </p>
    @endif
    
    <div class="row-fluid">
        <div class="span12">
          <div class="widget-box">
            <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
              <h5>Groups</h5>
            </div>
            <div class="widget-content nopadding">
              <table class="table table-bordered data-table">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Icon</th>
                    <th>Users</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($groups as $group)
                  
                  <tr id="group-{{$group->id}}" data-id="{{$group->id}}">
                    <td width="25%" id="group-name-box-{{$group->id}}" ><p id="group-name-p-tag-{{$group->id}}" style="font-size: 20px;" class="group-name-p-tag" >{{$group->name}}</p></td>
                    <td width="10%">
                        <img id="group-image-{{$group->id}}" alt="Group Icon" src="{{$group->icon}}" class="group-image" >
                        <div id="group-image-upload-{{$group->id}}" class="control-group group-image-upload" style="display:none;">
                            <form data-id="{{$group->id}}" id="group-image-upload-form" method="post" enctype="multipart/form-data">
                                <div class="controls">
                                {{ csrf_field() }}
                                <input id="groupid" type="hidden" name="groupid" class="required" value="{{$group->id}}"/>
                                <input id="grouplogo" type="file" name="grouplogo" class="required"/>
                                </div>
                                <button id="group-image-input-button-{{$group->id}}" type="submit" class="btn btn-success btn-mini">Save</button>
                                <a href="#" class="btn btn-danger btn-mini"  onclick="closeImage({{$group->id}}); return false;">Close</a>
                            </form>
                        </div>
                        <p id="logo-error-{{$group->id}}" class="logo-error" style="display:none">Please select an image to change the group icon.</p>
                    </td>
                    <td width="50%" class="blog_content">
                        <div id="member-table-div-{{$group->id}}" style="height: 300px;max-height: 300px;overflow: auto;">
                            <table id="member-table-{{$group->id}}" class="table table-bordered">
                            <tbody>
                            @foreach($group->users as $user)
                            <tr>
                            <td width="70%"><a href="{{ URL::to('view_usr_details/'.$user->userid ) }}" title="Click to view member profile">{{$user->title}}. {{$user->firstname}} {{$user->lastname}} </a></td>
                            <td width="30%" data-groupid="{{$group->id}}" id="groupuser-{{$user->groupuserid}}">@if($user->groupuserstatus) <button class="btn btn-success btn-mini" onclick="changegroupmemberStatus({{$user->groupuserid}})" title="Click to change the group member status">Active</button> @else <button class="btn btn-danger btn-mini" onclick="changegroupmemberStatus({{$user->groupuserid}})" title="Click to change the group member status">In Active</button> @endif</td>
                            </tr>
                            @endforeach
                            </tbody>
                            </table>
                        </div>
                    </td>
                    <td width="10%" id="group-status-{{$group->id}}">@if($group->status) <button class="btn btn-success" onclick="changegroupStatus({{$group->id}})" title="Click to change the group status">Active</button> @else <button class="btn btn-danger" onclick="changegroupStatus({{$group->id}})" title="Click to change the group status">In Active</button> @endif</td>
                    <td width="5%"><button class="btn btn-info" onClick="changegroupNameandImageandMember({{$group->id}})">Edit Group</button><br><br>
                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#groupModel" onclick="comfirmDeleteGroup('{{$group->id}}')">Delete Group</button>
                    </td>
                </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>   
        </div>
      </div>
  </div>

</div>
<!-- Modal -->
<div class="modal fade" id="groupModel" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 id="model_title" class="modal-title"></h4>
        </div>
        <div class="modal-body">
          <span id="modal_message" ></span>
        </div>
        <div class="modal-footer">
          <span id="modal_yes" ></span>
          <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
        </div>
      </div>
      
    </div>
  </div>
  
<!--end-main-container-part-->
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>

  $(document).ready(function(){
	$('input[type=checkbox],input[type=radio],input[type=file]').uniform();
    $('.data-table').dataTable({
      "bJQueryUI": true,
      "sPaginationType": "full_numbers",
      "sDom": '<""l>t<"F"fp>'
    });
    $("span.icon input:checkbox, th input:checkbox").click(function() {
      var checkedStatus = this.checked;
      var checkbox = $(this).parents('.widget-box').find('tr td:first-child input:checkbox');		
      checkbox.each(function() {
        this.checked = checkedStatus;
        if (checkedStatus == this.checked) {
          $(this).closest('.checker > span').removeClass('checked');
        }
        if (this.checked) {
          $(this).closest('.checker > span').addClass('checked');
        }
      });
    });	

    $('#group-image-upload-form').submit(function(event) {
        event.preventDefault();
        var formData = new FormData($(this)[0]);
        var groupid = $(this).attr('data-id');
        $.ajax({
            url: '{{ url('/changegroupicon') }}',
            type: 'POST',              
            data: formData,
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            success: function(result)
            {
                if(result == 0){
                    $('.logo-error').hide();
                    $('#logo-error-'+groupid).show();
                }else{
                    $('.logo-error').hide();
                    $('#group-image-upload-'+groupid).hide();
                    $('#group-image-'+groupid).attr("src",result);
                    $('#group-image-'+groupid).show();
                }
            },
            error: function(data)
            {
                location.reload();
            }
        });

    });
    $( ".required" ).on("change", function() {
      var v = $(this).val();
      var type = $(this).attr('type');
      if((v.replace(/\s+/g, '')) == '') {
        access = false;
        $(this).css("border", "2px solid rgb(255, 0, 0)");
      }else {
        $(this).css('border', '');
        if(type == 'file'){
          $('.logo-error').hide();
        }
      }
    });

    
  });
    function changegroupmemberStatus (groupuserid){
        var groupid = $('#groupuser-'+groupuserid).attr('data-groupid');
        var csrf_token = '{{ csrf_token() }}';
        $.post("changegroupmemberstatus",
            {
            _token: encodeURIComponent(csrf_token),
            groupuserid: groupuserid,
            groupid:groupid,
            },
            function(data){
                if(data == 1){
                    $('#groupuser-'+groupuserid).empty();
                    $('#groupuser-'+groupuserid).html('<button class="btn btn-success btn-mini" onclick="changegroupmemberStatus('+groupuserid+','+groupid+')" title="Click to change the group member status">Active</button>')
                }else if(data == 0){
                    $('#groupuser-'+groupuserid).empty();
                    $('#groupuser-'+groupuserid).html('<button class="btn btn-danger btn-mini" onclick="changegroupmemberStatus('+groupuserid+','+groupid+')" title="Click to change the group member status">In Active</button>') 
                }
            }
        );
    }

    function changegroupStatus (groupid){
        var csrf_token = '{{ csrf_token() }}';
        $.post("changegroupstatus",
            {
            _token: encodeURIComponent(csrf_token),
            groupid: groupid,
            
            },
            function(data){
                if(data == 1){
                    $('#group-status-'+groupid).empty();
                    $('#group-status-'+groupid).html('<button class="btn btn-success" onclick="changegroupStatus('+groupid+')" title="Click to change the group status">Active</button>')
                }else if(data == 0){
                    $('#group-status-'+groupid).empty();
                    $('#group-status-'+groupid).html('<button class="btn btn-danger" onclick="changegroupStatus('+groupid+')" title="Click to change the group status">In Active</button>') 
                }
            }
        );
    }
    

    function changegroupNameandImageandMember(groupid){
        $('.group-name-input').remove();
        $('.group-name-p-tag').show();
        $('.group-image-upload').hide();
        $('.logo-error').hide();
        $('.group-image').show();
        $('.group-member-box').remove();
        var box = $('#group-name-box-'+groupid);
        var p_tag = $('#group-name-p-tag-'+groupid);
        var img = $('#group-image-'+groupid);
        var img_upload = $('#group-image-upload-'+groupid);
        var name = p_tag.text();
        var table = $('#member-table-div-'+groupid);
        p_tag.hide();
        box.append('<div id="group-name-input-'+groupid+'" class="controls controls-row group-name-input"><input type="text" name="groupname" value="'+name+'" class="span12 m-wrap required"><button id="group-name-input-button-'+groupid+'" type="submit" class="btn btn-success btn-mini" onclick="savegroupName('+groupid+')">Save</button>&nbsp<button type="submit" class="btn btn-danger btn-mini" onclick="closeInput('+groupid+')">Close</button></div>')
        table.after('<div id="group-member-box-'+groupid+'" class="group-member-box"><br><a class="btn btn-info" target="_blank" href="{{url("addmembers")}}/'+groupid+'">Add Members</a>&nbsp<button type="submit" class="btn btn-danger" onclick="closeaddMember('+groupid+')">Close</button><div>')
        img.hide();
        img_upload.show(); 
    }

    function savegroupName (groupid){
        var csrf_token = '{{ csrf_token() }}';
        var value = $('input[name=groupname]').val();
        $.post("changegroupname",
            {
            _token: encodeURIComponent(csrf_token),
            groupid: groupid,
            groupname : value
            
            },
            function(data){
                
                if(data == 1){
                    $('.group-name-input').remove();
                    $('#group-name-p-tag-'+groupid).text(value);
                    $('#group-name-p-tag-'+groupid).show();
                }else if(data == 0){
                    $('input[name=groupname]').css("border", "2px solid rgb(255, 0, 0)");
                }
            }
        );
    }

    function closeInput(groupid){
        $('.group-name-input').remove();
        $('#group-name-p-tag-'+groupid).show();
    }
    function closeImage(groupid){
        $('.logo-error').hide();
        $('.group-image-upload').hide();
        $('#group-image-'+groupid).show();
    }
    function closeaddMember(groupid){
        $('.group-member-box').remove();
    }
    function comfirmDeleteGroup(groupid)
    {
        var ModalMessage = '<p>Are you sure you want to delete this group ?</p>';
        var ModalYes = '<button type="button" onclick="deleteGroup('+groupid+')" class="btn btn-success" data-dismiss="modal">Yes</button>';
        var ModelTitle = "Confirm Delete"
        $("#modal_message").html(ModalMessage);
        $("#modal_yes").html(ModalYes);
        $("#model_title").text(ModelTitle);
        
    }

    function deleteGroup(groupid)
    {
        var csrf_token = '{{ csrf_token() }}';
        $.post("deletegroup",
            {
            _token: encodeURIComponent(csrf_token),
            groupid: groupid,
            
            },
            function(data){
                if(data){
                    $("#group-"+groupid).remove();
                }
            }
        );

    } 
</script>


@endsection