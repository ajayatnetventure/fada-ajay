@extends('AdminLayout.admin_frame')

@section('content')





<!--main-container-part-->

<div id="content">

  <!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
  </div>
  <!--End-breadcrumbs-->
    
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
            <h5>Manufacturers</h5>
          </div>
          <div class="widget-content nopadding">
          <table class="table table-bordered data-table">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Designation</th>
                    <th>Company</th>
                    <th>Dealer of</th>
                    <th>Address</th>
                    <th>Email</th>
                    <th>Mobile</th>
                    <th>Status</th>
                    <th style="width:200px !important;" >Action</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($users as $user)
                  <tr class="table_row" id="user-{{$user->id}}" data-id="{{$user->id}}">
                    <td>{{$user->title}}. {{$user->firstname}} {{$user->lastname}}</td>
                    <td>{{$user->designation}}</td>
                    <td>{{$user->dealer}}</td>
                    <td>{{$user->manufacturer}}</td>
                    <td>{{$user->address1}}, {{$user->address2}},<br>{{$user->city}},<br>{{$user->state}},<br>PIN: {{$user->pin}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->mobile}}</td>
                    <td id="user-status-{{$user->id}}">@if($user->status) <button class="btn btn-success btn-mini" onclick="changeuserStatus({{$user->id}})" title="Click to change the user status">Active</button> @else <button class="btn btn-danger btn-mini" onclick="changeuserStatus({{$user->id}})" title="Click to change the user status">Inactive</button> @endif</td>
                    <td style="width:200px !important;"  >
                      <a href="update_usr?id={{$user->id}}" ><div class="btn btn-success" >edit</div></a>
                      <div class="btn btn-danger" onclick="change_modal_content({{$user->id}})" data-toggle="modal" data-target="#myModal" >delete</div>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
          </div>
        </div>   

      </div>
    </div>
  </div>

</div>
<!--end-main-container-part-->




  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Confirm</h4>
        </div>
        <div class="modal-body">
          <span id="modal_message" ></span>
        </div>
        <div class="modal-footer">
          <span id="modal_yes" ></span>
          <button type="button" class="btn btn-danger" data-dismiss="modal">no</button>
        </div>
      </div>
      
    </div>
  </div>




<script>
  $(document).ready(function(){
	
    $('.data-table').dataTable({
      "bJQueryUI": true,
      "sPaginationType": "full_numbers",
      "sDom": '<""l>t<"F"fp>'
    });


  });

  function change_modal_content(usr_id)
  {
    var ModalMessage = '<p>Are you sure you want to delete this User ?</p>';
    var ModalYes = '<button type="button" onclick="send_delete_req('+usr_id+')" class="btn btn-success" data-dismiss="modal">Yes</button>';

    $("#modal_message").html(ModalMessage);
    $("#modal_yes").html(ModalYes);
    
  }


  function send_delete_req(usr_id)
  {

    //$("#row"+usr_id).hide();

    var csrf_token = $('meta[name="csrf-token"]').attr('content');
    
    $.post("delete_usr",
      {
        _token: encodeURIComponent(csrf_token),
        userid: usr_id,
        
      },
      function(data){
        if(data == 1){
            $('#user-'+usr_id).remove();
        }
      }
    ); 

  }  
  function changeuserStatus (userid){
        var csrf_token = '{{ csrf_token() }}';
        $.post("changeuserstatus",
            {
            _token: encodeURIComponent(csrf_token),
            userid: userid,
            
            },
            function(data){
                if(data == 1){
                    $('#user-status-'+userid).empty();
                    $('#user-status-'+userid).html('<button class="btn btn-success btn-mini" onclick="changeuserStatus('+userid+')" title="Click to change the user status">Active</button>')
                }else if(data == 0){
                    $('#user-status-'+userid).empty();
                    $('#user-status-'+userid).html('<button class="btn btn-danger btn-mini" onclick="changeuserStatus('+userid+')" title="Click to change the user status">Inactive</button>') 
                }
            }
        );
    }


</script>



@endsection