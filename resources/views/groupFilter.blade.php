@extends('AdminLayout.admin_frame')

@section('content')


<!--main-container-part-->

<div id="content">

  <!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
  </div>
  <!--End-breadcrumbs-->
    
  
<div  class="container-fluid">

  <hr>
    @if(Session::has('group-success'))
    <p style="color:green;text-align:center;font-size: 13px;" >
        {{ Session::get('group-success') }}
    </p>
    @endif
    @if(Session::has('group-error'))
    <p style="color:red;text-align:center;font-size: 13px;" >
    {{ Session::get('group-error') }}
    </p>
    @endif
  <div class="row-fluid">
    <div class="span12">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Select a Filter</h5>
        </div>
        <div class="widget-content nopadding">
          <div class="form-horizontal">
          {{ csrf_field() }}
            <div class="control-group">
              <label class="control-label">Filters :</label>
              <div class="controls">
                <select id="filterdata" name="filterdata">
                    <option value="">Select a Filter</option>  
                    @foreach ($filters as $data)
                    <option value="{{$data['id']}}">{{$data['filterdata']}}</option>
                    @endforeach
                </select> 
              </div>
            </div>
        </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row-fluid">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title bg_ly" data-toggle="collapse" href="#collapseG2"><span class="icon"><i class="icon-chevron-down"></i></span>
                <h5 id="filterelementname">Select Required Filter Elements</h5>
                
            </div>
            <div class="widget-content nopadding collapse in" id="collapseG2">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="pull-right filter-search">
                            <input id="filter-search" type="text" placeholder="Search..">
                        </div>
                    </div>
                </div> 
                <div class="row-fluid">
                    <div class="span12">   
                        <div id="filterelements"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
  <div class="row-fluid">
    <div class="span12">
        <div class="widget-box">
            <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                <h5>Selected Filter Elements</h5>
                
            </div>
            <div class="widget-content nopadding collapse in" id="collapseG2">
                <div id="selectedfilterelements"></div>
            </div>
        </div>
    </div>
  </div>
  <div class="row-fluid">
    <div class="span12">
        <div class="widget-box">
            <form id="filterelementsform" action="selectmembers" method="post" class="form-horizontal">
                {{ csrf_field() }}
                <div class="form-actions">
                <input id="filterbutton" type="submit" class="btn btn-success pull-left" value="Save" />
                </div>
            </form>
        </div>
    </div>
  </div>
</div>  

</div>

<!--end-main-container-part-->
<script>
    $(document).ready(function () {
        $( "#filterdata" ).on("change", function() {
            var filter = $('#filterdata').find(":selected").val();
            if(filter){
                getFilterElements(filter);
            }
            
        });  
        $("#filter-search").keyup(function(){
            var filter = $(this).val()
            if(filter){
                $("#filtercheckbox label").each(function(){
                    if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                        $(this).css("display","none");
                    } else {
                        $("#loadMore").hide();
                        $(this).removeAttr('style');
                    }
                });
            }else{
                $('.select-filter-box').css("display","none");
                $(".select-filter-box").filter(function() {
                    if($(this).attr("data-filter-id") <= 25){
                        $(this).removeAttr('style');
                        $("#loadMore").show();
                    }
                });
            }
            
        });
    });
    function getFilterElements(filter)
    {
        var csrf_token = '{{ csrf_token() }}';
        $.post("getfilterelements",
            {
            _token: encodeURIComponent(csrf_token),
            filter: filter,
            
            },
            function(data){
                if(data){
                    $('#filterelements').empty();
                    var filter = $('#filterdata').find(":selected").text();
                    var filterid = $('#filterdata').find(":selected").val();
                    $('#filterelementname').html('Select Required "'+filter+'" Elements');
                    var html = '<div data-toggle="buttons" id="filtercheckbox">';
                    var values = [];
                    $('.hidden_'+filterid).each(function(){
                        values.push(this.value); 
                    });
                    var i = 1;
                    var link = "";
                    jQuery.each(data, function(index, item) {
                        var check = "";
                        var display = "";
                        var id = '' + this['id'];
                        if (jQuery.inArray(id, values)!='-1') {
                            check = "checked";
                        }
                        if(i>25){
                            display = 'style="display:none;"';
                            link = '<p class="filter-select"><button id="loadMore" type="button" class="btn btn-primary pull-right" onclick="loadMoreFilters(25)">Load More</button></p>'

                        }
                       
                        html += '<label id="select-filter-box-'+i+'" class="btn btn-primary filterelements select-filter-box" '+display+' data-filter-id="'+i+'"><input name="'+this['name']+'" type="checkbox" '+check+' autocomplete="off" data-id="'+this['id']+'"> '+this['name']+' </label>';
                        i++;
                    });
                    html += "</div>";
                    html +=link;
                    html += '<p class="filter-select"><button type="button" class="btn btn-success filter-select-button" onclick="getFilters('+filterid+')">Select Filter</button></p>'
                    $('#filterelements').append(html);
                }
            }
        );

    } 

    function getFilters(filterid){
        var filter = $("#filterdata option[value='"+filterid+"']").text()
        var selected = [];
        $('input.hidden_' + filterid).remove();
        $('#filtercheckbox input:checked').each(function() {
            selected.push($(this).attr('name'));
            passtoInput(filterid,this);
        });
        
        if ( $( "#selected_"+filterid ).length ) {
            $( "#selected_"+filterid ).remove();
        }
        if(selected.length !=0){
            var html = '<div id="selected_'+filterid+'" data-id="'+filterid+'">';
            html += '<p class="filter-selected-elements">Selected "'+filter+'" Elements</p>';
            html += '<div class="show-select-filter-box">';
            jQuery.each(selected, function(index, item) {
                html += '<label class="btn btn-primary filterelements">'+this+' </label>';
            });
            html += "</div>";
            html += "</div>";
            $('#selectedfilterelements').append(html);
        }
        
    } 

    function passtoInput(filter, elements){
        $('#filterelementsform').prepend('<input class="hidden_'+filter+'" name="'+filter+'[]" type="hidden" value="'+$(elements).attr('data-id')+'">');
    }

    function loadMoreFilters(offset){
        for(var i = offset+1; i<=offset+25; i++){
            if( $('#select-filter-box-'+i).length )
            {
                $('#select-filter-box-'+i).removeAttr('style');
           }else{
            $("#loadMore").remove();
                return false;
           }
        }
        $("#loadMore").attr("onclick","loadMoreFilters("+(offset+25)+")");
        
    }
</script>

@endsection