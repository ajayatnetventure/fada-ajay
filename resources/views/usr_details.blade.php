@extends('AdminLayout.admin_frame')

@section('content')

<style>

.custom_user_detalis{ background-color:;margin-top:5px; }

</style>

<!--main-container-part-->
<div id="content">

  <!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
  </div>
  <!--End-breadcrumbs-->
    
  <div  class="container-fluid">

    <hr>

    <div class="row-fluid">
      <div class="span6">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
            <h5>User Details</h5>
          </div>
          <div class="widget-content nopadding">
            <div class="form-horizontal" style="padding:20px;" >

             <div class="control-group" >
               <label class="control-label">First Name :</label>
               <div class="controls">
                 <div class="span11 custom_user_detalis"  >
                   {{$data[0]['firstname']}}
                 </div>
               </div>
             </div>
             <div class="control-group" >
               <label class="control-label">Last Name :</label>
               <div class="controls">
                 <div class="span11 custom_user_detalis" >
                 {{$data[0]['lastname']}}
                 </div>
               </div>
             </div>
             <div class="control-group" >
               <label class="control-label">Gender  :</label>
               <div class="controls">
                 <div class="span11 custom_user_detalis">
                 {{$data[0]['gender']}}
                 </div>
               </div>
             </div>
             <div class="control-group" >
               <label class="control-label">UserName   :</label>
               <div class="controls">
                 <div class="span11 custom_user_detalis">
                 {{$data[0]['username']}}
                 </div>
               </div>
             </div>
             <div class="control-group" >
               <label class="control-label">Password    :</label>
               <div class="controls">
                 <div class="span11 custom_user_detalis">
                 {{$data[0]['password']}}
                 </div>
               </div>
             </div>
             <div class="control-group" >
               <label class="control-label">Email     :</label>
               <div class="controls">
                 <div class="span11 custom_user_detalis">
                 {{$data[0]['email']}}
                 </div>
               </div>
             </div>
             <div class="control-group" >
               <label class="control-label">Mobile No    :</label>
               <div class="controls">
                 <div class="span11 custom_user_detalis">
                 {{$data[0]['mobile']}}
                 </div>
               </div>
             </div>
             <div class="control-group" >
               <label class="control-label">Phone No    :</label>
               <div class="controls">
                 <div class="span11 custom_user_detalis">
                 {{$data[0]['phonenumber']}}
                 </div>
               </div>
             </div>
             <div class="control-group" >
               <label class="control-label">Manufacture     :</label>
               <div class="controls">
                 <div class="span11 custom_user_detalis">
                 {{$data[0]['manufacturename']}}
                 </div>
               </div>
             </div>
             <div class="control-group" >
               <label class="control-label">Dealer      :</label>
               <div class="controls">
                 <div class="span11 custom_user_detalis">
                 {{$data[0]['dealername']}}
                 </div>
               </div>
             </div>
             <div class="control-group" >
               <label class="control-label">Designation      :</label>
               <div class="controls">
                 <div class="span11 custom_user_detalis">
                 {{$data[0]['designationname']}}
                 </div>
               </div>
             </div>
             <div class="control-group" >
               <label class="control-label">Address 1      :</label>
               <div class="controls">
                 <div class="span11 custom_user_detalis">
                 {{$data[0]['address1']}}
                 </div>
               </div>
             </div>
             <div class="control-group" >
               <label class="control-label">Address 2      :</label>
               <div class="controls">
                 <div class="span11 custom_user_detalis">
                 {{$data[0]['address2']}}
                 </div>
               </div>
             </div>
             <div class="control-group" >
               <label class="control-label">Pin      :</label>
               <div class="controls">
                 <div class="span11 custom_user_detalis">
                 {{$data[0]['pin']}}
                 </div>
               </div>
             </div>
             <div class="control-group" >
               <label class="control-label">State      :</label>
               <div class="controls">
                 <div class="span11 custom_user_detalis">
                 {{$data[0]['statename']}}
                 </div>
               </div>
             </div>
             <div class="control-group" >
               <label class="control-label">City       :</label>
               <div class="controls">
                 <div class="span11 custom_user_detalis">
                 {{$data[0]['cityname']}}
                 </div>
               </div>
             </div>



            </div>  
          </div>
        </div>
      </div>
    </div> 

  </div>

</div>
<!--main-container-end-->


@endsection