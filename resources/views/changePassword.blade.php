@extends('AdminLayout.admin_frame')

@section('content')


<!--main-container-part-->

<div id="content">

  <!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{url('dashboard')}}" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
  </div>
  <!--End-breadcrumbs-->
      
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span6">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
            <h5>Change Administrator Password</h5>
          </div>
          <div class="widget-content nopadding">
            <form id="changepasswordform" action="changepassword" method="post" class="form-horizontal">
            {{ csrf_field() }}
              <div class="control-group">
                <label class="control-label">Old Password :</label>
                <div class="controls">
                  <input id="oldpassword" name="oldpassword" type="password" class="span11 required" placeholder="Old Password" />
                  {!! $errors->first('oldpassword', '<p style="color:red;text-align:center;font-size: 13px;" >:message</p>') !!}
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">New Password :</label>
                <div class="controls">
                  <input id="newpassword" name="newpassword" type="password" class="span11 required" placeholder="New Password" />
                  {!! $errors->first('newpassword', '<p style="color:red;text-align:center;font-size: 13px;" >:message</p>') !!}
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Confirm Password :</label>
                <div class="controls">
                  <input id="confirmpassword" name="confirmpassword" type="password"  class="span11 required" placeholder="Confirm Password"  />
                  {!! $errors->first('confirmpassword', '<p style="color:red;text-align:center;font-size: 13px;" >:message</p>') !!}
                </div>
              </div>
               @if(Session::has('admin-success'))
                <p style="color:green;text-align:center;font-size: 13px;" >
                    {{ Session::get('admin-success') }}
                </p>
                @endif
                @if(Session::has('admin-error'))
                <p style="color:red;text-align:center;font-size: 13px;" >
                {{ Session::get('admin-error') }}
                </p>
                @endif
              <div class="form-actions">
                <button type="submit" class="btn btn-success pull-right">Change Administrator Password</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>  



</div>

<!--end-main-container-part-->
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>
    $(document).ready(function () {
        $('#changepasswordform').validate({ 
            rules: {
                oldpassword: {
                    required: true
                },
                newpassword: {
                    required: true
                },
                confirmpassword: {
                    required: true,
                    equalTo : "#newpassword"
                }
            },
            errorPlacement: function(error, element) {
                if (element.attr("name") == "oldpassword" ) {
                    $('#oldpassword').css("border", "2px solid rgb(255, 0, 0)");
                }
                if (element.attr("name") == "newpassword" ) {
                    $('#newpassword').css("border", "2px solid rgb(255, 0, 0)");
                }
                if (element.attr("name") == "confirmpassword" ) {
                    $('#confirmpassword').css("border", "2px solid rgb(255, 0, 0)");
                }
            }

        }); 
        $( ".required" ).on("change", function() {
            var v = $(this).val();
            if((v.replace(/\s+/g, '')) == '') {
                access = false;
                $(this).css("border", "2px solid rgb(255, 0, 0)");
            }else {
                $(this).css('border', '');
            }
        });    
  });
</script>


@endsection