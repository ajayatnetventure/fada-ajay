@extends('AdminLayout.admin_frame')

@section('content')

<!--main-container-part-->

<div id="content">

  <!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
  </div>
  <!--End-breadcrumbs-->
    
  
<div  class="container-fluid">

  <hr>
  <div class="row-fluid">
    <div class="span6">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Edit Manufacturer</h5>
        </div>
        <div class="widget-content nopadding">
          <form id="mfrformupdate" action="update_mfr" method="post" class="form-horizontal">
          {{ csrf_field() }}
            <div class="control-group">
              <label class="control-label">Manufacturer Name :</label>
              <div class="controls">
                <input id="mfr_id" style="display:none;" name="mfr_id" class="span11"  />
                <input id="mfr_name" name="mfr_name" type="text" class="span11 required"  />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Manufacturer Type :</label>
              <div class="controls">
                  <?php
                     foreach($data['vehicles'] as $vehicle)
                     {
                       echo'<input type="checkbox"  id="vehicle_type_'.$vehicle["id"].'"  name="vehicle_type[]" value="'.$vehicle["id"].'" style="margin:5px;" class="required" >'.$vehicle["vehicletype"].'<br>';
                     }
                  ?>
              </div>
              <p id="vehicle_type" style="color:red;text-align:center;font-size: 13px;display:none;" >Please select vehicle type.</p>
            </div>
            @if(Session::has('mfr-success'))
                <p style="color:green;text-align:center;font-size: 13px;" >
                    {{ Session::get('mfr-success') }}
                </p>
                @endif
                @if(Session::has('mfr-error'))
                <p style="color:red;text-align:center;font-size: 13px;" >
                {{ Session::get('mfr-error') }}
                </p>
                @endif
            <div class="form-actions">
              <input id="mfr_button" type="submit" class="btn btn-success pull-right" value="Save" />
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>  

</div>

<!--end-main-container-part-->



<!-- main js -->
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>
  var mfr = <?php if(!empty($data)){ echo json_encode($data); } ?>;
  var name = mfr.mfr_name;
  var id = mfr.id;
  $(document).ready(function(){
      $('#mfr_name').val(name);
      $('#mfr_id').val(id);
      var  veh_input_id;
      mfr['selected_veh'].forEach( function(elements)
      {   
          veh_input_id = 'vehicle_type_'+elements; 
          document.getElementById(veh_input_id).checked = true;
      });


      $('#mfrformupdate').validate({ 
        rules: {
          mfr_name: {
                required: true
            },
            'vehicle_type[]': {
                required: true,
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "mfr_name" ) {
                $('#mfr_name').css("border", "2px solid rgb(255, 0, 0)");
            }
            if (element.attr("name") == "vehicle_type[]" ) {
                $('#vehicle_type').show();
            }
        }

    }); 
  $( ".required" ).on("change", function() {
      var v = $(this).val();
      var type = $(this).attr('type');
      if((v.replace(/\s+/g, '')) == '') {
        access = false;
        $(this).css("border", "2px solid rgb(255, 0, 0)");
      }else {
        $(this).css('border', '');
        if(type == 'checkbox'){
          if (($("input[name*='vehicle_type']:checked").length)<=0) {
            $('#vehicle_type').show();
          }else{
            $('#vehicle_type').hide();
          }
        }
      }
    }); 

      

    
  });


</script>

<!-- end main js ->



@endsection