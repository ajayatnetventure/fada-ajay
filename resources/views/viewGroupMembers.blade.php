@extends('AdminLayout.admin_frame')

@section('content')

<!--main-container-part-->

<div id="content">

  <!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
  </div>
  <!--End-breadcrumbs-->
    
  <div class="container-fluid">
    <hr>
    <form id="memberselectform" action="creategroup" method="post" class="form-horizontal" enctype="multipart/form-data">
      <div class="row-fluid">
        <div class="span6">
          <div class="widget-box">
            <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
              <h5>Add Group Name</h5>
            </div>
            <div class="widget-content nopadding">
              {{ csrf_field() }}
              <div class="control-group">
                <label class="control-label">Group Name :</label>
                <div class="controls">
                  <input name="groupname" id="groupname" type="text" class="span11 required" placeholder="Name" />
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="span6">
          <div class="widget-box">
            <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
              <h5>Create New Group</h5>
            </div>
            <div class="widget-content nopadding">
              {{ csrf_field() }}
              <div class="control-group">
                <label class="control-label">File upload input</label>
                <div class="controls">
                  <input id="grouplogo" type="file" name="grouplogo" class="required"/>
                </div>
                <p id="logo-error" class="logo-error" style="display:none">Please select an icon for the new group.</p>
              </div>
          </div>
        </div>
        
      </div>
    
      <div class="row-fluid">
        <div class="span12">
          <div class="widget-box">
            <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
              <h5>Select Members</h5>
            </div>
            <div class="widget-content nopadding">
              <table class="table table-bordered data-table">
                <thead>
                  <tr>
                    <th><input type="checkbox" id="title-table-checkbox" name="title-table-checkbox" /></th>
                    <th>Name</th>
                    <th>Designation</th>
                    <th>Company</th>
                    <th>Dealer of</th>
                    <th>Address</th>
                    <th>Email</th>
                    <th>Mobile</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($users as $user)
                  <tr id="user-{{$user->id}}" data-id="{{$user->id}}">
                    <td><input type="checkbox" /></td>
                    <td>{{$user->title}}. {{$user->firstname}} {{$user->lastname}}</td>
                    <td>{{$user->designation}}</td>
                    <td>{{$user->dealer}}</td>
                    <td>{{$user->manufacturer}}</td>
                    <td>{{$user->address1}}, {{$user->address2}},<br>{{$user->city}},<br>{{$user->state}},<br>PIN: {{$user->pin}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->mobile}}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>   
        </div>
      </div>
      <div class="row-fluid">
        <div class="span12">
          <div class="widget-box">
            <input id="memberselectinput" name="users" type="hidden" value="">
            <div class="form-actions">
              <input id="createbutton" type="submit" class="btn btn-success pull-right" value="Create New Group" />
            </div>   
          </div>
        </div>
      </div>
    </form>
  </div>

</div>
<!--end-main-container-part-->
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>

  $(document).ready(function(){
	
    $('.data-table').dataTable({
      "bJQueryUI": true,
      "sPaginationType": "full_numbers",
      "sDom": '<""l>t<"F"fp>'
    });
    $('input[type=checkbox],input[type=radio],input[type=file]').uniform();
    $("span.icon input:checkbox, th input:checkbox").click(function() {
      var checkedStatus = this.checked;
      var checkbox = $(this).parents('.widget-box').find('tr td:first-child input:checkbox');		
      checkbox.each(function() {
        this.checked = checkedStatus;
        if (checkedStatus == this.checked) {
          $(this).closest('.checker > span').removeClass('checked');
        }
        if (this.checked) {
          $(this).closest('.checker > span').addClass('checked');
        }
      });
    });	
    $('table [type="checkbox"]').change(function(){
      var users = [];
      $('table [type="checkbox"]').each(function(i, chk) {
        if (chk.checked) {
          users.push($(chk).closest('tr').attr('data-id'));
        }
      });
      $('#memberselectinput').val(users);
      if(users.length != 0){
        $('#member-error').remove();
      }else{
        $('#createbutton').after('<p id="member-error" style="color:red;font-size: 13px;">Please select members for the new group.</p>');
      }
    });
  
    $('#memberselectform').validate({ 
      ignore: [],
      rules: {
          users: {
              required: true
          },
          groupname: {
              required: true
          },
          grouplogo: {
              required: true
          }
      },
      errorPlacement: function(error, element) {
          if (element.attr("name") == "groupname" ) {
              $('#groupname').css("border", "2px solid rgb(255, 0, 0)");
          }
          if (element.attr("name") == "grouplogo" ) {
              $('#logo-error').show();
          }
          if (element.attr("name") == "users" ) {
            $('#member-error').remove();
            $('#createbutton').after('<p id="member-error" style="color:red;font-size: 13px;">Please select members for the new group.</p>');
          }
      }
    }); 
    $( ".required" ).on("change", function() {
      var v = $(this).val();
      var type = $(this).attr('type');
      
      if((v.replace(/\s+/g, '')) == '') {
        access = false;
        $(this).css("border", "2px solid rgb(255, 0, 0)");
      }else {
        $(this).css('border', '');
        if(type == 'file'){
          $('#logo-error').hide();
        }
      }
    }); 
   
  });


</script>



@endsection