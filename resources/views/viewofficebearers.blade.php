@extends('AdminLayout.admin_frame')

@section('content')

<!--main-container-part-->

<div id="content">

  <!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
  </div>
  <!--End-breadcrumbs-->
    
  
<div  class="container-fluid">

  <hr>
  <div class="row-fluid">
    <div class="span6">
      <div id="titlebox"  class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Select Office Bearer Title</h5>
        </div>
        <div class="widget-content nopadding">
          <form action="#" class="form-horizontal">
            <div class="control-group">
              <label class="control-label">Office Bearer Title :</label>
              <div class="controls">
                <select id="bearer_title" name="bearer_title" class="required" >
                    <option value="" >Select Office Bearer Title</option>;         
                    <option value="1" >President</option>;
                    <option value="2" >Vice President</option>;
                    <option value="3" >Secretary</option>;
                    <option value="4" >Treasurer</option>;
                </select> 
              </div>
            </div>
          </form>
        </div>
      </div>
      <div id="typebox"  class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>National / State</h5>
        </div>
        <div class="widget-content nopadding">
          <form action="#" class="form-horizontal">
            <div class="control-group">
              <label class="control-label">National / State :</label>
              <div class="controls">
                <select id="bearer_type" name="bearer_type" class="required" >
                    <option value="" >Select National / State</option>;         
                    <option value="1" >National</option>;
                    <option value="0" >State</option>;
                </select> 
              </div>
            </div>
          </form>
        </div>
      </div>
      <div id="statebox" class="widget-box" style="display:none;">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>State</h5>
        </div>
        <div class="widget-content nopadding">
          <form action="#" class="form-horizontal">
            <div class="control-group">
              <label class="control-label">State :</label>
              <div class="controls">
                <select id="state" name="state" class="required" >
                    <option value="" >Select State</option>;
                    @foreach($states as $state)         
                        <option value="{{$state->id}}" >{{$state->statename}}</option>;
                    @endforeach
                </select> 
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="widget-box">
        <div class="widget-content nopadding">
          <form action="#" class="form-horizontal">
            <div class="form-actions">
              <input id="view-office-bearers" type="button" class="btn btn-info pull-right" value="View"/>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="span6">
    <div class="widget-box">
      <div class="widget-title bg_ly" data-toggle="collapse" href="#collapseG2"><span class="icon"><i class="icon-chevron-down"></i></span>
        <h5 id="displayTitle">Office Bearers</h5>
      </div>
      <div class="widget-content nopadding">
            <table class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>Member</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody id="displayBody">
                
              </tbody>
            </table>
          </div>
    </div>
    </div>
  </div>
  
</div>  

</div>
<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Confirm</h4>
        </div>
        <div class="modal-body">
          <span id="modal_message" ></span>
        </div>
        <div class="modal-footer">
          <span id="modal_yes" ></span>
          <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
        </div>
      </div>
      
    </div>
  </div>
<!--end-main-container-part-->
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>
    $(document).ready(function () {
      
        $( ".required" ).on("change", function() {
        var v = $(this).val();

        if((v.replace(/\s+/g, '')) == '') {
            access = false;
            $(this).css("border", "2px solid rgb(255, 0, 0)");
        }else {
            $(this).css('border', '');
        }
        });  

        $( "#bearer_type" ).on("change", function() {
            var val = $(this).val();
            if(val == 1 || !val){
                $('#statebox').hide();
            }else{
                $('#statebox').show();
            }
        }); 

        $('#view-office-bearers').click(function (e) {
            e.preventDefault();
            var title = $('#bearer_title').val();
            var type = $('#bearer_type').val();
            var state = $('#state').val();
            if(!title){
                $('#bearer_title').css("border", "2px solid rgb(255, 0, 0)"); 
                return false;
            }
            if(!type){
                $('#bearer_type').css("border", "2px solid rgb(255, 0, 0)"); 
                return false;
            }
            if(type == 0 && !state){
                $('#state').css("border", "2px solid rgb(255, 0, 0)");
                return false; 
            }
            submitForm(title,type,state);
        }); 
    });

  function comfirmDelete(id)
  {
    var ModalMessage = '<p>Are you sure you want to remove this office bearer ?</p>';
    var ModalYes = '<button type="button" onclick="deleteofficeBearer('+id+')" class="btn btn-success" data-dismiss="modal">Yes</button>';

    $("#modal_message").html(ModalMessage);
    $("#modal_yes").html(ModalYes);
    
  }


function deleteofficeBearer(id)
{
  var csrf_token = '{{ csrf_token() }}';
  $.post("deleteofficebearer",
    {
      _token: encodeURIComponent(csrf_token),
      id: id,
      
    },
    function(data){
      if(data == 1){
        $('#office-bearers-'+id).remove();
      }
    }
  );

} 

function submitForm(title = NULL,type = NULL,state = NULL){
    var csrf_token = '{{ csrf_token() }}';
    $.post("officebearerajax",
        {
        _token: encodeURIComponent(csrf_token),
        title: title,
        type: type,
        state: state,
        },
        function(data){
            if(data !=0){
                var title = $('#bearer_title option:selected').text();
                var type = $('#bearer_type option:selected').text();
                var state = $('#state option:selected').text();
                var text = 'Office Bearers';
                if(title !="Select Office Bearer Title"){text +=' - '+title}
                if(type !="Select National / State"){text +=' - '+type}
                if(state !="Select State"){text +=' - '+state}
                $('#displayTitle').text(text);
                data =  JSON.parse(data);
                $('#displayBody').empty();
                var html = '';
                jQuery.each(data, function(index, item) {
                    html += '<tr id="office-bearers-'+item.id+'">';
                    html +='<td class="taskDesc"><a href="{{url("view_usr_details")}}/'+item.id+'" target="_blank">'+item.username+'. '+item.firstname+' '+item.lastname+'</a></td>';
                    html +='<td class="taskStatus"><div class="btn btn-danger btn-mini" onclick="comfirmDelete('+item.id+')" data-toggle="modal" data-target="#myModal" >Delete</div></td>'
                    html +='</tr>'
                    
                });
                $('#displayBody').html(html);
            }
        }
    );
}

</script>

@endsection