@extends('AdminLayout.admin_frame')

@section('content')

<!--main-container-part-->

<div id="content">

  <!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
  </div>
  <!--End-breadcrumbs-->
    
  <div class="container-fluid">
    <hr>
      @if(Session::has('mfr-view-error'))
      <p style="color:red;text-align:center;font-size: 13px;" >
      {{ Session::get('mfr-view-error') }}
      </p>
      @endif
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
            <h5>Manufacturers</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>Manufacturer Name</th>
                  <th>Vehicle Types</th>
                  <th>Status</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                  @foreach($mfrs as $mfr)
                  <tr id="mfr-{{$mfr->id}}" data-id="{{$mfr->id}}">
                    <td>{{$mfr->manufacturername}}</td>
                    <td>
                      @foreach($mfr->type as $type)
                        {{$type}}<br>
                      @endforeach
                    </td>
                    <td id="mfr-status-{{$mfr->id}}">@if($mfr->status) <button class="btn btn-success btn-mini" onclick="changemfrStatus({{$mfr->id}})" title="Click to change the manufacturer status">Active</button> @else <button class="btn btn-danger btn-mini" onclick="changemfrStatus({{$mfr->id}})" title="Click to change the manufacturer status">In Active</button> @endif</td>
                    <td>
                      <a href="update_mfr?id={{$mfr->id}}" ><div class="btn btn-success" >Edit</div></a>
                      <div class="btn btn-danger" onclick="change_modal_content({{$mfr->id}})" data-toggle="modal" data-target="#myModal" >Delete</div>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
            </table>
          </div>
        </div>   


      </div>
    </div>
  </div>

</div>
<!--end-main-container-part-->




  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Confirm</h4>
        </div>
        <div class="modal-body">
          <span id="modal_message" ></span>
        </div>
        <div class="modal-footer">
          <span id="modal_yes" ></span>
          <button type="button" class="btn btn-danger" data-dismiss="modal">no</button>
        </div>
      </div>
      
    </div>
  </div>




<script>
  $(document).ready(function(){
	
  $('.data-table').dataTable({
    "bJQueryUI": true,
    "sPaginationType": "full_numbers",
    "sDom": '<""l>t<"F"fp>'
  });
});

  function change_modal_content(mfr_id)
  {
    var ModalMessage = '<p>Are you sure you want to delete this Manufacturer ?</p>';
    var ModalYes = '<button type="button" onclick="send_delete_req('+mfr_id+')" class="btn btn-success" data-dismiss="modal">Yes</button>';

    $("#modal_message").html(ModalMessage);
    $("#modal_yes").html(ModalYes);
    
  }


  function send_delete_req(mfr_id)
  {
    var csrf_token = $('meta[name="csrf-token"]').attr('content');
    
    $.post("delete_mfr",
    {
      _token: encodeURIComponent(csrf_token),
      mfrid: mfr_id,
      
    },
    function(data){
        if(data == 1){
            $('#mfr-'+mfr_id).remove();
        }
      }); 

  }  

  function changemfrStatus (mfrid){
        var csrf_token = '{{ csrf_token() }}';
        $.post("changemfrstatus",
            {
            _token: encodeURIComponent(csrf_token),
            mfrid: mfrid,
            
            },
            function(data){
                if(data == 1){
                    $('#mfr-status-'+mfrid).empty();
                    $('#mfr-status-'+mfrid).html('<button class="btn btn-success btn-mini" onclick="changemfrStatus('+mfrid+')" title="Click to change the manufacturer status">Active</button>')
                }else if(data == 0){
                    $('#mfr-status-'+mfrid).empty();
                    $('#mfr-status-'+mfrid).html('<button class="btn btn-danger btn-mini" onclick="changemfrStatus('+mfrid+')" title="Click to change the manufacturer status">In Active</button>') 
                }
            }
        );
    }


</script>



@endsection