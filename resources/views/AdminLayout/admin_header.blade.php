
<!--Header-part-->
<div id="header"  >
<h1><a href="{{url('dashboard')}}">FADA Admin</a></h1>
</div>
<!--close-Header-part--> 

<!--top-Header-menu-->
<div id="user-nav" class="navbar navbar-inverse">
  <ul class="nav">
    <li class="dropdown" id="menu-messages"><a href="#" data-toggle="dropdown" data-target="#menu-messages" class="dropdown-toggle"><i class="icon icon-cogs"></i> <span class="text">Settings</span><i class="fas fa-chevron-down"></i> <b class="caret"></b></a>
      <ul class="dropdown-menu">
        <li><a class="sAdd" title="" href="{{url('changepassword')}}"><i class="icon-key"></i> Change Password</a></li>
      </ul>
    </li>
    <li class=""><a title="" href="{{url('logout')}}"><i class="icon icon-lock"></i> <span class="text">Logout</span></a></li>
  </ul>
</div>
<!--close-top-Header-menu-->

