
<?php
  $session = session('fada_admin');
?>


<!--sidebar-menu-->
<div id="sidebar"><a href="#" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>
  <ul>
    <li ><a href="{{url('dashboard')}}" ><i class="icon icon-home"></i> <span>Dashboard</span></a> </li>

@if($session['admintype'] == 0)    

    <li class="content" id="forms_main7" onclick="show_options('')" > 
      <a href="{{url('createfilter')}}"><i class="icon icon-th-list"></i> <span>Filter</span></a>
    </li>

    <li class="content" id="forms_main1" onclick="show_options('mfr')" > <a href="#"><i class="icon icon-th-list"></i> <span>Manufacturer</span> <span class="label label-important">2</span></a>
      <ul class="forms_sub" id="forms_sub_mfr" >
        <li><a href="{{url('create_mfr')}}"><i class="icon icon-home"></i> <span id="li_mfr_create" >create mfr</span></a> </li>
        <li><a href="{{url('view_mfr')}}"><i class="icon icon-home"></i> <span id="li_mfr_view" >view mfr</span></a> </li>
      </ul>
    </li>

    <li class="content" id="forms_main9" onclick="show_options('veh')" > <a href="#"><i class="icon icon-th-list"></i> <span>Vehicle</span> <span class="label label-important">2</span></a>
      <ul class="forms_sub" id="forms_sub_veh" >
        <li ><a href="{{url('create_veh')}}" ><i class="icon icon-home"></i> <span id="li_veh_create" >create veh type</span></a> </li>
        <li ><a href="{{url('view_veh')}}" ><i class="icon icon-home"></i> <span id="li_veh_view" >view veh type</span></a> </li>               
      </ul>
    </li>   

    <li class="content" id="forms_main2" onclick="show_options('dlr')" > <a href="#"><i class="icon icon-th-list"></i> <span>Dealer</span> <span class="label label-important">2</span></a>
      <ul class="forms_sub"  id="forms_sub_dlr" >
        <li><a href="{{url('create_dlr')}}"><i class="icon icon-home"></i> <span id="li_dlr_create" >create dealer</span></a> </li>
        <li><a href="{{url('view_dlr')}}"><i class="icon icon-home"></i> <span id="li_dlr_view" >view dlr</span></a> </li>
      </ul>
    </li>
    
    <li class="content" id="forms_main3" onclick="show_options('dsg')" > <a href="#"><i class="icon icon-th-list"></i> <span>Designation</span> <span class="label label-important">2</span></a>
      <ul class="forms_sub" id="forms_sub_dsg" >
        <li><a href="{{url('create_dsg')}}"><i class="icon icon-home"></i> <span id="li_dsg_create" >create dsg</span></a> </li>
        <li ><a href="{{url('view_dsg')}}"><i class="icon icon-home"></i> <span id="li_dsg_view" >view dsg</span></a> </li>
      </ul>
    </li>

    <li class="content" id="forms_main5" onclick="show_options('ad')" > <a href="#"><i class="icon icon-th-list"></i> <span>Admin</span> <span class="label label-important">2</span></a>
      <ul class="forms_sub" id="forms_sub_ad" >
        <li ><a href="{{url('create_ad')}}"><i class="icon icon-home"></i> <span id="li_ad_create" >create admin</span></a> </li>
        <li ><a href="{{url('view_ad')}}"><i class="icon icon-home"></i> <span id="li_ad_view" >admin details</span></a> </li>               
      </ul>
    </li>   

    <li class="content" id="forms_main8" onclick="show_options('ofc')" > <a href="#"><i class="icon icon-th-list"></i> <span>OfficerBearer</span> <span class="label label-important">2</span></a>
      <ul class="forms_sub" id="forms_sub_ofc" >
        <li ><a href="{{url('addofficebearers')}}" ><i class="icon icon-home"></i> <span id="li_ofc_create" >create OfficerBearer</span></a> </li>
        <li ><a href="{{url('viewofficebearers')}}" ><i class="icon icon-home"></i> <span id="li_ofc_view" >view OfficerBearer</span></a> </li>               
      </ul>
    </li>   



@endif

    <li class="content" id="forms_main4" onclick="show_options('usr')" > <a href="#"><i class="icon icon-th-list"></i> <span>User</span> <span class="label label-important">2</span></a>
      <ul class="forms_sub" id="forms_sub_usr" >
        <li ><a href="{{url('create_usr')}}"><i class="icon icon-home"></i> <span id="li_usr_create" >create user</span></a> </li>
        <li><a href="{{url('view_usr?s=43')}}"><i class="icon icon-home"></i> <span id="li_usr_view" >view user</span></a> </li>
      </ul>
    </li>

    <li class="content" id="forms_main6" onclick="show_options('grp')" > <a href="#"><i class="icon icon-th-list"></i> <span>Group</span> <span class="label label-important">2</span></a>
      <ul class="forms_sub" id="forms_sub_grp" >
        <li ><a href="{{url('creategroup')}}"><i class="icon icon-home"></i> <span id="li_grp_create" >create group</span></a> </li>
        <li ><a href="{{url('viewgroups')}}"><i class="icon icon-home"></i> <span id="li_grp_view" >view groups</span></a> </li>                
      </ul>
    </li>
  </ul>



</div>
<!--sidebar-menu-->






<script>

    var sidebar = <?php if(Session::has('admin-success')){  $arr = Session::get('admin-success');echo json_encode($arr);  }else{  $arr = array('section'=>'none','activity'=>'none') ;echo json_encode($arr); }  ?>;


    $(document).ready(function(){

      control_side_onload(sidebar.section,sidebar.activity);
      
    });


    var id,prev_id = sidebar.section;
    function show_options(section)
    { 
      
      if(prev_id != section)
      {
        id = '#forms_sub_' + section;
        $('.forms_sub').slideUp();
        $(id).slideDown();
      }
      // console.log('prev :'+prev_id,'current :'+section);
      prev_id = section;
      
      
    }



    var active_ul_id, active_li_id;
    function control_side_onload(section,activity)
    {
      active_ul_id =  '#forms_sub_'+section;
      active_li_id = '#li_'+section+'_'+activity;
      
      $('.forms_sub').hide();
      $(active_ul_id).show();
      $(active_li_id).addClass('text-success');
    }

</script>