<!DOCTYPE html>
<html lang="en">
<head>
    <title>FADA Administrator</title>
        
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="apple-touch-icon" sizes="180x180" href="{{url('public/img/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{url('public/img/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{url('public/img/favicon-16x16.png')}}">
    <link rel="manifest" href="{{url('public/img/site.webmanifest')}}">
    <link rel="mask-icon" href="{{url('public/img/safari-pinned-tab.svg')}}" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">


    <link rel="stylesheet" href="{{url('public/css/bootstrap.min.css')}}" />
    <link rel="stylesheet" href="{{url('public/css/bootstrap-responsive.min.css')}}" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" href="{{url('public/css/fullcalendar.css')}}" />
    <link rel="stylesheet" href="{{url('public/css/matrix-style.css')}}" />
    <link rel="stylesheet" href="{{url('public/css/matrix-media.css')}}" />
    <link rel="stylesheet" href="{{url('public/css/jquery.gritter.css')}}" />
    <link rel="stylesheet" href="{{url('public/css/uniform.css')}}" />
    <link rel="stylesheet" href="{{url('public/css/custom.css')}}" />

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />  

    <link href="{{url('public/font-awesome/css/font-awesome.css')}}" rel="stylesheet" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
        
    <script src="{{url('public/js/jquery.min.js')}}"></script> 

    <script src="https://cdn.bootcss.com/Uniform.js/4.2.2/js/jquery.uniform.bundled.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="{{url('public/js/bootstrap.min.js')}}"></script> 
    



</head>
<body>

@include('AdminLayout/admin_header')

@include('AdminLayout/admin_sidebar')

@yield('content')

@include('AdminLayout/admin_footer')


</body>
</html>
