@extends('AdminLayout.admin_frame')

@section('content')

<style>

.error_border{ display:none }

</style>


<!--main-container-part-->

<div id="content">

  <!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
  </div>
  <!--End-breadcrumbs-->
    

<div  class="container-fluid">

  <hr>
  <div class="row-fluid">
    <div class="span6">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Create A New User</h5>
        </div>
        <div class="widget-content nopadding">
          <form action="create_usr" method="post" class="form-horizontal">
            {{ csrf_field() }}
            <div class="control-group">
              <label class="control-label">First Name :</label>
              <div class="controls">           
                <input name="first_name" id="first_name" type="text" class="span11 required" placeholder="FirstName" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Last Name :</label>
              <div class="controls">
                <input name="last_name" id="last_name" type="text" class="span11 required" placeholder="LastName" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Gender :</label>
              <div class="controls">
              <input type="radio" name="gender" value="male"> Male
              <input type="radio" name="gender" value="female"> Female
              </div>
            </div>   
            <div class="control-group">
              <label class="control-label ">Date of Birth :</label>
              <div class="controls">
              <input type="date" name="dob" class="required" >
              </div>
            </div>      
            <div class="control-group">
              <label class="control-label">UserName :</label>
              <div class="controls ">
                <input name="user_name" id="user_name" type="text" class="span11 required" placeholder="UserName" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Password :</label>
              <div class="controls">
                <input name="password" id="password" type="text" class="span11 required" placeholder="Password" />
              </div>
            </div>

            <div class="control-group">
              <label class="control-label">Email :</label>
              <div class="controls">
                <input name="email" id="email" type="text" class="span11 required" placeholder="Email" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Mobile No :</label>
              <div class="controls">
                <input name="mobile_no" id="mobile_no" type="text" class="span11 required" placeholder="MobileNo" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Phone No :</label>
              <div class="controls">
                <input name="std_code" id="std_code" type="text" class="span11 required" placeholder="STD Code" style="width:20%;" />
                <input name="phone" id="phone" type="text" class="span11 required" placeholder="PhoneNumber"  style="width:50%;" />
              </div>
            </div>

            <div class="control-group">
              <label class="control-label">Manufacturer :</label>
              <div class="controls">
                <select name="manufacture_type"  id="manufacture_type" class="manufacture_type required" >
                  <option value="" ><div >select a manufacturer</div></option>
                  <?php
                     foreach($data['manufacturers'] as $state)
                     {
                      echo '<option value="'.$state['id'].'"><div >'.$state['manufacturername'].'</div></option>';
                     }
                  ?>
                </select> 
              </div>
            </div>

            <div class="control-group">
              <label class="control-label">Vehicle Type :</label>
              <div class="controls">
                <select name="vehicle_type"  id="vehicle_type" class="vehicle_type required" >

                </select> 
              </div>
            </div>

            <div class="control-group">
              <label class="control-label">Dealer :</label>
              <div class="controls">
                <select name="dealer_type" id="dealer_type" class="required" >
                  <option value="" ><div >select a dealer</div></option>
                  <?php
                      foreach($data['dealers'] as $dealer)
                      {
                        echo '<option value="'.$dealer['id'].'"><div >'.$dealer['dealername'].'</div></option>';
                      }
                  ?>
                </select> 
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Designation :</label>
              <div class="controls">
                <select name="designation_type"  id="designation_type" class="required" >
                  <option value="" ><div >select a designation</div></option>
                  <?php
                      foreach($data['designations'] as $state)
                      {
                        echo '<option value="'.$state['id'].'"><div >'.$state['designationname'].'</div></option>';
                      }
                  ?>
                </select> 
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Address 1 :</label>
              <div class="controls">
                <input name="address1" id="address1" type="text" class="span11 required" placeholder="Address1" />
              </div>
            </div>
            <div class="control-group" >
              <label class="control-label">Address 2 :</label>
              <div class="controls">
                <input  name="address2" id="address2" type="text" class="span11 required" placeholder="Address2" />
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Pin :</label>
              <div class="controls">
                <input name="pin" id="pin" type="text" class="span11 required" placeholder="Pin" />
              </div>
            </div>

            <div class="control-group">
              <label class="control-label">State :</label>
              <div class="controls">
                <select id="state" name="state" class="state required" >
                    <option value="" ><div >select a state</div></option>
                    <?php
                      foreach($data['states'] as $state)
                      {
                        echo '<option value="'.$state['id'].'"><div >'.$state['statename'].'</div></option>';
                      }
                    ?>
                </select> 
              </div>
            </div>

            <div class="control-group">
              <label class="control-label">City :</label>
              <div class="controls">
                <select id="city" name="city" class="required" >
                
                </select> 
              </div>
            </div>

            <div class="form-actions">
              <input  id="usr_button" type="submit" class="btn btn-success pull-right" value="Save" />
            </div>

          </form>
        </div>
      </div>
    </div>
  </div>
  <p style="color:green;width:50%;text-align:center;" ><?php if(!empty($data['message'])){ echo $data['message']; } ?></p>
</div>  

</div>

<!--end-main-container-part-->



<script>

  var prev_state_id = 0;
  var current_state_id;

  $(document).ready(function(){



    $("#state").click(function(){
      current_state_id = $(".state option:selected").val();

      if(prev_state_id != current_state_id)
      {
        update_city(current_state_id);
      }

      prev_state_id = current_state_id;

    });

    


    var current_mfr_id;
    var prev_mfr_id = 0;

    $("#manufacture_type").click(function(){
      current_mfr_id = $(".manufacture_type option:selected").val();

      if(prev_mfr_id != current_mfr_id)
      {
        update_type(current_mfr_id);
      }

      prev_mfr_id = current_mfr_id;

    });




  });





function update_city(dlr_id)
{

  var csrf_token = $('meta[name="csrf-token"]').attr('content');
  var dis_option;
  var dis_array;

  $.get(

      "state_data",
      {
        _token: encodeURIComponent(csrf_token),
        state_id: dlr_id,
        
      },
      function(data) 
      { 
             
        if(data)
        {
          dis_array = data;
          $('#city').html('<option value="" ><div >select a city</div></option>');
           
           dis_array.forEach(function (element)
           {
               dis_option = '<option value="'+element['id']+'">'+element['cityname']+'</option>';
               $('#city').append(dis_option);
           }
           );    
           
        }

      }

  ); 

}  





function update_type(id)
{

  var csrf_token = $('meta[name="csrf-token"]').attr('content');
  var dis_option;
  var dis_array;

  $.get(

      "type_data",
      {
        _token: encodeURIComponent(csrf_token),
        mfr_id: id,
        
      },
      function(data) 
      { 
             
        if(data)
        {
          dis_array = data;
          $('#vehicle_type').html('<option value="" ><div >select a type</div></option>');
           
           dis_array.forEach(function (element)
           {
                dis_option = '<option value="'+element['veh_type_id']+'">'+element['veh_type_name']+'</option>';
                $('#vehicle_type').append(dis_option);
              
           }
           );          
        }

      }

  ); 

}  


</script>







<script>
//validate the form


var form_key,submit_key;
var required_fields = document.getElementsByClassName('required');

$(":submit").click(function(){ validate_form(); });

$( ".required" ).on("change", function() { 
   
  if(submit_key == 1)
  {
    validate_form();
  }

});



function validate_form()
{ 

  form_key = 0;
  submit_key = 1;

  for(var i =0;i < required_fields.length;i++)
  {
    if(required_fields[i].value == "")
    {        
      required_fields[i].style.border = "2px solid rgb(255, 0, 0)";
      form_key = 1;
    }
    else
    {
      required_fields[i].style.border = "1px solid #a9abad";
    }
  } 

  if(form_key == 1)
  {
    $("form").submit(function(e){
        e.preventDefault(e);
    });  
  }
  else
  {
    $("form").submit(function(e){
      $(this).unbind('submit').submit();
    });  
  }
  
}



//validate the form
</script>








@endsection