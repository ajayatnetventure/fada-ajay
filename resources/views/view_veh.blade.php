@extends('AdminLayout.admin_frame')

@section('content')


<!--main-container-part-->

<div id="content">

  <!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
  </div>
  <!--End-breadcrumbs-->
    
  <div class="container-fluid">
    <hr>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
            <h5>Dealers</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>Dealer Name</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                  @foreach($data as $veh)
                    <tr id="veh-{{$veh->id}}" data-id="{{$veh->id}}">
                        <td>{{$veh->vehicletype}}</td>
                        <td>
                        <a href="update_veh?id={{$veh->id}}" ><div class="btn btn-success" >Edit</div></a>
                        </td>
                    </tr>
                  @endforeach
              </tbody>
            </table>
          </div>
        </div>      
      </div>
    </div>
  </div>

</div>
<!--end-main-container-part-->



  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Confirm</h4>
        </div>
        <div class="modal-body">
          <span id="modal_message" ></span>
        </div>
        <div class="modal-footer">
          <span id="modal_yes" ></span>
          <button type="button" class="btn btn-danger" data-dismiss="modal">no</button>
        </div>
      </div>
      
    </div>
  </div>




<script>
$(document).ready(function(){
	
  $('.data-table').dataTable({
    "bJQueryUI": true,
    "sPaginationType": "full_numbers",
    "sDom": '<""l>t<"F"fp>'
  });
});

function change_modal_content(veh_id)
{
  var ModalMessage = '<p>Are you sure you want to delete this Vehicle Type ?</p>';
  var ModalYes = '<button type="button" onclick="send_delete_req('+veh_id+')" class="btn btn-success" data-dismiss="modal">Yes</button>';

  $("#modal_message").html(ModalMessage);
  $("#modal_yes").html(ModalYes);
  
}



function send_delete_req(veh_id)
{

  var csrf_token = $('meta[name="csrf-token"]').attr('content');
  
  $.post("delete_veh",
  {
    _token: encodeURIComponent(csrf_token),
    vehid: veh_id,
    
  },
    function(data){
        if(data == 1){
            $('#veh-'+veh_id).remove();
        }
      }); 

}  

function changevehStatus (vehid){
        var csrf_token = '{{ csrf_token() }}';
        $.post("changevehstatus",
            {
            _token: encodeURIComponent(csrf_token),
            vehid: vehid,
            
            },
            function(data){
                if(data == 1){
                    $('#veh-status-'+vehid).empty();
                    $('#veh-status-'+vehid).html('<button class="btn btn-success btn-mini" onclick="changevehStatus('+vehid+')" title="Click to change the manufacturer status">Active</button>')
                }else if(data == 0){
                    $('#veh-status-'+vehid).empty();
                    $('#veh-status-'+vehid).html('<button class="btn btn-danger btn-mini" onclick="changevehStatus('+vehid+')" title="Click to change the manufacturer status">In Active</button>') 
                }
            }
        );
    }

</script>


@endsection