@extends('AdminLayout.admin_frame')

@section('content')

<!--main-container-part-->

<div id="content">

  <!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
  </div>
  <!--End-breadcrumbs-->
    
  
<div  class="container-fluid">

  <hr>
  <div class="row-fluid">
    <div class="span6">
      <div class="widget-box">
        <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
          <h5>Add New Filter</h5>
        </div>
        <div class="widget-content nopadding">
          <form id="filterform" action="createfilter" method="post" class="form-horizontal">
            {{ csrf_field() }}
            <div class="control-group">
              <label class="control-label">Filters :</label>
              <div class="controls">
                <select id="filterdata" name="filterdata" class="required">
                    <option value="">Select a Filter</option>  
                    <option value="Manufacturer(Make)">Manufacturer(Make)</option>
                    <option value="Vehicle Type">Vehicle Type</option>
                    <option value="Designation of Member">Designation of Member</option>
                    <option value="Dealer">Dealer</option>
                    <option value="State">State</option>
                    <option value="City">City</option>
                    <option value="Office Bearers">Office Bearers</option>
                </select> 
              </div>
            </div>
            @if(count($errors) != 0)
              <p style="color:red;text-align:center;font-size: 13px;" >
                  {{str_replace(array("[","]",'"'),' ',$errors)}}
              </p>
            @endif
            @if($success)
              <p style="color:green;text-align:center;font-size: 13px;" >
                  <!-- {{str_replace(array("[","]",'"'),' ',$errors)}} -->
                  {{$success}}
              </p>
            @endif
            <div class="form-actions">
              <input id="filterbutton" type="submit" class="btn btn-success pull-right" value="Save" />
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="span6">
    <div class="widget-box">
      <div class="widget-title bg_ly" data-toggle="collapse" href="#collapseG2"><span class="icon"><i class="icon-chevron-down"></i></span>
        <h5>Filters Added</h5>
      </div>
      <div class="widget-content nopadding collapse in" id="collapseG2">
        <ul class="recent-posts">
        @foreach ($datas as $data)
          <li id="filter{{$data['id']}}">
            <div class="article-post"> <p class="filter-data">{{$data['filterdata']}} </p>
            <p class="filter-data"><button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#myModal" onclick="comfirmDelete('{{$data['id']}}')">Delete Filter</button></p>
            </div>
          </li>
          @endforeach  
        </ul>
      </div>
    </div>
    </div>
  </div>
  
</div>  

</div>
<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Confirm</h4>
        </div>
        <div class="modal-body">
          <span id="modal_message" ></span>
        </div>
        <div class="modal-footer">
          <span id="modal_yes" ></span>
          <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
        </div>
      </div>
      
    </div>
  </div>
<!--end-main-container-part-->
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>
    $(document).ready(function () {
      $('#filterform').validate({ 
        rules: {
            filterdata: {
                required: true
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "filterdata" ) {
                $('#filterdata').css("border", "2px solid rgb(255, 0, 0)");
            }
        }

    }); 
    $( ".required" ).on("change", function() {
      var v = $(this).val();
      if((v.replace(/\s+/g, '')) == '') {
        access = false;
        $(this).css("border", "2px solid rgb(255, 0, 0)");
      }else {
        $(this).css('border', '');
      }
    });    
  });

  function comfirmDelete(id)
  {
    var ModalMessage = '<p>Are you sure you want to delete this filter ?</p>';
    var ModalYes = '<button type="button" onclick="deleteFilter('+id+')" class="btn btn-success" data-dismiss="modal">Yes</button>';

    $("#modal_message").html(ModalMessage);
    $("#modal_yes").html(ModalYes);
    
  }


function deleteFilter(id)
{
  var csrf_token = '{{ csrf_token() }}';
  $.post("deletefilter",
    {
      _token: encodeURIComponent(csrf_token),
      id: id,
      
    },
    function(data){
      if(data){
        $("#filter"+id).remove();
      }
    }
  );

}  


</script>

@endsection